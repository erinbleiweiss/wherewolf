# Wherewolf game DAO
# Abstraction for the SQL database access.

import psycopg2
import md5


class UserAlreadyExistsException(Exception):
    def __init__(self, err):
        self.err = err
    def __str__(self):
        return 'Exception: ' + self.err
        
class NoUserExistsException(Exception):
    def __init__(self, err):
        self.err = err
    def __str__(self):
        return 'Exception: ' + self.err
        
class BadArgumentsException(Exception):
    """Exception for entering bad arguments"""
    def __init__(self, err):
        self.err = err
    def __str__(self):
        return 'Exception: ' + self.err

class WherewolfDao:

    def __init__(self, dbname='wherewolf', pgusername='wherewolf', pgpasswd='wherewolf'):
        self.dbname = dbname
        self.pgusername = pgusername
        self.pgpasswd = pgpasswd
        self.pghostname = "wherewolf.cswsgfborksq.us-west-2.rds.amazonaws.com"
        print ('connection to database {}, user: {}, password: {}'.format(dbname, pgusername, pgpasswd))

    def get_db(self):
        return psycopg2.connect(database=self.dbname,user=self.pgusername,password=self.pgpasswd, host=self.pghostname)

    def create_user(self, username, password, firstname, lastname, avatar):
        """ registers a new player in the system """
        conn = self.get_db()
        with conn:
            c = conn.cursor()
            c.execute('SELECT COUNT(*) from gameuser WHERE username=%s',(username,))
            n = int(c.fetchone()[0])
            # print 'num of rfdickersons is ' + str(n)
            if n == 0:
                hashedpass = md5.new(password).hexdigest()
                c.execute('INSERT INTO gameuser (username, password, firstname, lastname, avatar) VALUES (%s,%s,%s,%s,%s)', 
                          (username, hashedpass, firstname, lastname, avatar))
                conn.commit()
            else:
                raise UserAlreadyExistsException('{} user already exists'.format((username)) )
        
    def check_password(self, username, password):
        """ return true if password checks out """
        conn = self.get_db()
        with conn:
            c = conn.cursor()
            sql = ('select password from gameuser where username=%s')
            c.execute(sql,(username,))
            hashedpass = md5.new(password).hexdigest()
            u = c.fetchone()
            if u == None:
                raise NoUserExistsException(username)
            # print 'database contains {}, entered password was {}'.format(u[0],hashedpass)
            return u[0] == hashedpass
        
    def set_location(self, username, lat, lng):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            sql = ('update player set lat=%s, lng=%s '
                   'where player_id=(select current_player from gameuser '
                   'where username=%s)')
            cur.execute(sql, (lat, lng, username))
            conn.commit()


    def get_location(self, username):
        conn = self.get_db()
        result = {}
        with conn:
            c = conn.cursor()
            sql = ('select player_id, lat, lng from player, gameuser '
                   'where player.player_id = gameuser.current_player '
                   'and gameuser.username=%s')
            c.execute(sql, (username,))
            row = c.fetchone()
            result["playerid"] = row[0]
            result["lat"] = row[1]
            result["lng"] = row[2]
        return result

        
    def get_alive_nearby(self, username, game_id, radius): 
        ''' returns all alive players near a player '''
        conn = self.get_db()
        result = []
        with conn:
            c = conn.cursor()
            # using the radius for lookups now
            sql = ('select player_id from player where '
                   'earth_box(ll_to_earth( '
                   '(select lat from player, gameuser '
                   'where player.player_id = gameuser.current_player '
                   'and gameuser.username=%s), '
                   '(select lng from player, gameuser '
                   'where player.player_id = gameuser.current_player '
                   'and gameuser.username=%s)), %s) '
                   '@> ll_to_earth(lat, lng) '
                   'and is_dead = 0 '
                   'and game_id = %s'
            ) 

            # sql = ('select username, player_id, point( '
            #       '(select lng from player, gameuser '
            #       'where player.player_id=gameuser.current_player '
            #       'and gameuser.username=%s), '
            #       '(select lat from player, gameuser '
            #       'where player.player_id=gameuser.current_player '
            #       'and gameuser.username=%s)) '
            #       '<@> point(lng, lat)::point as distance, '
            #       'is_werewolf '
            #       'from player, gameuser where game_id=%s '
            #       'and is_dead=0 '
            #       'and gameuser.current_player=player.player_id '
            #       'order by distance')
            # print sql
            c.execute(sql, (username, username, radius, game_id))
            for row in c.fetchall():
                d = {}
                d["player_id"] = row[0]
                #d["distance"] = row[1]
                #d["is_werewolf"] = row[2]
                result.append(d)
        return result

    def add_item(self, username, itemname):
        conn = self.get_db()
        with conn:
            cur=conn.cursor()

            cmdupdate = ('update inventory set quantity=quantity+1'
                         'where itemid=(select itemid from item where name=%s)' 
                         'and playerid='
                         '(select current_player from gameuser where username=%s);')
            cmd = ('insert into inventory (playerid, itemid, quantity)' 
                   'select (select current_player from gameuser where username=%s) as cplayer,'
                   '(select itemid from item where name=%s) as item,' 
                   '1 where not exists' 
                   '(select 1 from inventory where itemid=(select itemid from item where name=%s)' 
                   'and playerid=(select current_player from gameuser where username=%s))')
            cur.execute(cmdupdate + cmd, (itemname, username, username, itemname, itemname, username))

            conn.commit()

 
    def remove_item(self, username, itemname):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            cmd = ('update inventory set quantity=quantity-1 where ' 
                   'itemid=(select itemid from item where name=%s) and ' 
                   'playerid=(select current_player from gameuser where username=%s);')
            cmddelete = ('delete from inventory where itemid=(select itemid from item where name=%s)' 
                         'and playerid=(select current_player from gameuser where username=%s) '
                         'and quantity < 1;')
            cur.execute(cmd + cmddelete, (itemname, username, itemname, username))
            conn.commit()


    def get_items(self, username):
        conn = self.get_db()
        items = []
        with conn:
            c = conn.cursor()
            sql = ('select item.name, item.description, item.damage, item.armor, inventory.quantity '
                   'from item, inventory, gameuser where '
                   'inventory.itemid = item.itemid and '
                   'gameuser.current_player=inventory.playerid and '
                   'gameuser.username=%s')
            c.execute(sql, (username,))
            for item in c.fetchall():
                d = {}
                d["name"] = item[0]
                d["description"] = item[1]
                d["damage"] = item[2]
                d["armor"] = item[3]
                d["quantity"] = item[4]
                items.append(d)
        return items

        
    def award_achievement(self, username, achievementname):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            cmd = ('insert into user_achievement (user_id, achievement_id, created_at) '
                   'values ((select user_id from gameuser where username=%s), '
                   '(select achievement_id from achievement where name=%s), now());')
            cur.execute(cmd, (username, achievementname))
            conn.commit()

        
    def get_achievements(self, username):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            cmd = ('select name, description, created_at from achievement, user_achievement '
                   'where achievement.achievement_id = user_achievement.achievement_id '
                   'and user_achievement.user_id = '
                   '(select user_id from gameuser where username=%s);')
            cur.execute(cmd, (username,))
            achievements = []
            for row in cur.fetchall():
                d = {}
                d["name"] = row[0]
                d["description"] = row[1]
                d["created_at"] = row[2]
                achievements.append(d)
        return achievements

    def award_stat(self, username, stat_name, stat_value):
        conn = self.get_db()
        with conn:
            cur=conn.cursor()

            cmdupdate = ('update user_stat set stat_value=stat_value+%s'
                         'where stat_name=%s' 
                         'and user_id='
                         '(select user_id from gameuser where username=%s);')
            cmd = ('insert into user_stat (user_id, stat_name, stat_value)' 
                   'select (select user_id from gameuser where username=%s) as c_user,'
                   '%s as stat_name,' 
                   '1 where not exists' 
                   '(select 1 from user_stat where stat_name=%s' 
                   'and user_id=(select user_id from gameuser where username=%s))')
            cur.execute(cmdupdate + cmd, (stat_value, stat_name, username, username, stat_name, stat_name, username))

            conn.commit()


    def set_dead(self, username):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            cmd = ('update player set is_dead=1 '
                   'where player_id='
                   '(select current_player from gameuser where username=%s);')
            cur.execute(cmd, (username,))
            conn.commit()


    def get_players(self, gameid):
        conn = self.get_db()
        players = []
        with conn:
            cur = conn.cursor()
            cmd = ('select player_id, is_dead, lat, lng, is_werewolf from player '
                   ' where game_id=%s;')
            cur.execute(cmd, (gameid,))
            for row in cur.fetchall():
                p = {}
                p["playerid"] = row[0]
                p["is_dead"] = row[1]
                p["lat"] = row[2]
                p["lng"] = row[3]
                p["is_werewolf"] = row[4]
                players.append(p)
        return players

    def get_player_names(self, gameid):
        conn = self.get_db()
        players = []
        with conn:
            cur = conn.cursor()
            cmd = ('select gameuser.firstname, gameuser.current_player, gameuser.avatar '
                    'from gameuser, player where gameuser.current_player=player.player_id '
                    'and player.game_id=%s')
            cur.execute(cmd, (gameid,))
            for row in cur.fetchall():
                p = {}
                p["playername"] = row[0]
                p["playerid"] = row[1]
                p["avatar"] = row[2]
                players.append(p)

        return players

    def get_user_stats(self, username):
        pass
	    
        
    def get_player_stats(self, username):
        pass

    # game methods    
    def join_game(self, username, gameid):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            cmd = ('INSERT INTO player ( is_dead, lat, lng, game_id) '
                   'VALUES ( %s, %s, %s, %s) returning player_id')
            cmd2 = ('update gameuser set current_player=%s where username=%s')
            cur.execute(cmd,( 0, 0, 0, gameid))
            cur.execute(cmd2, (cur.fetchone()[0], username));
            conn.commit()
	
    def leave_game(self, username):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            cmd1 = '''UPDATE gameuser set current_player = null where username=%s'''
            cur.execute(cmd1, (username,)) 
            conn.commit()

    def leave_game_lobby(self, username):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            cmd1 = ('delete from player using gameuser where '
                    'player.player_id=gameuser.current_player and '
                    'gameuser.username=%s')
            cmd2 = ('update gameuser set current_player=null where username=%s')
            cur.execute(cmd1, (username,))
            cur.execute(cmd2, (username,))
            conn.commit()
	    
        
    def create_game(self, username, gamename, description):
        ''' returns the game id for that game '''
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            cmd = ('INSERT INTO game (admin_id, name, description) VALUES ( '
                   '(SELECT user_id FROM gameuser where username=%s), '
                   '%s, %s) returning game_id')
            cur.execute(cmd,(username, gamename, description,))
            game_id = cur.fetchone()[0]
            conn.commit()
            return game_id


    def game_info(self, game_id):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            cmd = '''SELECT game_id, admin_id, status, name from game where game_id=%s'''
            cur.execute(cmd, (game_id,))
            row = cur.fetchone()
            d = {}
            d["game_id"] = row[0]
            d["admin_id"] = row[1]
            d["status"] = row[2]
            d["name"] = row[3]
            return d

    def get_games(self):
        conn = self.get_db()
        games = []
        with conn:
            cur = conn.cursor()
            cmd = ("SELECT game.game_id, game.name, game.status, gameuser.firstname from game, gameuser where game.admin_id = gameuser.user_id")
            cur.execute(cmd)
            for row in cur.fetchall():
                d = {}
                d["game_id"] = row[0]
                d["name"] = row[1]
                d["status"] = row[2]
                d["admin_name"] = row[3]
                games.append(d)
        return games

            
    def set_game_status(self, game_id, status):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            cmd = ('UPDATE game set status=%s '
                   'where game_id=%s')
            cur.execute(cmd, (game_id, status))
        

    def vote(self, game_id, username, target_username):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            sql = ('insert into vote '
                   '(game_id, player_id, target_id, cast_date) '
                   'values ( %s,'
                   '(select current_player from gameuser where username=%s), '
                   '(select current_player from gameuser where username=%s), '
                   'now())')
            cur.execute(sql, (game_id, username, target_username))
            conn.commit()


    def vote_by_target(self, game_id, target_id):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            sql = ('select username from gameuser, vote where '
                    'gameuser.current_player=vote.player_id and '
                    'vote.target_id=%s and vote.game_id=%s')
            cur.execute (sql, (target_id, game_id,))
            names = cur.fetchall()

            players = []
            for row in names:
                players.append(row[0])

            return players 
    
    def clear_tables(self):
        conn = self.get_db()
        with conn:
            c = conn.cursor()
            c.execute('truncate gameuser cascade')
            c.execute('truncate player cascade')
            c.execute('truncate user_achievement cascade')
            conn.commit()


    def get_userid(self, username):        
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            cmd = '''SELECT user_id from gameuser where username=%s'''
            cur.execute(cmd, (username,))
            user_id = cur.fetchone()[0]

            return user_id


    def get_playerid(self, username):        
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            cmd = '''SELECT current_player from gameuser where username=%s'''
            cur.execute(cmd, (username,))
            player_id = cur.fetchone()[0]

            return player_id


    def get_username(self, player_id):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            cmd = '''SELECT username from gameuser where current_player=%s'''
            cur.execute(cmd, (player_id,))
            username = cur.fetchone()[0]

            return username


    def get_gameid(self, username):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            cmd = ('select game_id from player where player_id=(select user_id from gameuser where username=%s)')
            cur.execute(cmd, (username,))
            game_id = cur.fetchone()

            if game_id != None:
                game_id = game_id[0]
            
            return game_id

    def count_votes(self, game_id):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            cmd = ('select target_id from vote where game_id=%s')
            cur.execute(cmd, (game_id,))

            votes = {}
            for i in cur.fetchall():
                i = i[0]
                if i not in votes:
                    votes[i] = 1
                else:
                    votes[i] += 1

        tally = []
        for key, value in votes.iteritems():
            temp = {"playerid": key, "votes": value}
            tally.append(temp)

        return tally


    def set_werewolf(self, player_id, werewolf):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            sql = ('update player set is_werewolf=%s '
                   'where player_id=%s')
            cur.execute(sql, (werewolf, player_id,))
            conn.commit()

    def get_stats (self, username):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            sql = ('select attack_stat.hp, attack_stat.attack from '
                    'attack_stat, player, gameuser where '
                    'attack_stat.rank=player.is_werewolf and '
                    'player.player_id=gameuser.current_player '
                    'and gameuser.username=%s')
            cur.execute(sql, (username,))
            row = cur.fetchone()
            stats = {}
            stats["hp"] = int(row[0])
            stats["attack"] = int(row[1])

            return stats


    def give_player_stat(self, username, stat_name):
        conn = self.get_db()
        with conn:
            cur=conn.cursor()

            cmdupdate = ('update player_stat set stat_value=stat_value+1'
                         'where stat_name=%s' 
                         'and player_id='
                         '(select current_player from gameuser where username=%s);')
            cmd = ('insert into player_stat (player_id, stat_name, stat_value)' 
                   'select (select current_player from gameuser where username=%s) as cplayer,'
                   '%s as item,' 
                   '1 where not exists' 
                   '(select 1 from player_stat where stat_name=%s' 
                   'and player_id=(select current_player from gameuser where username=%s))')
            cur.execute(cmdupdate + cmd, (stat_name, username, username, stat_name, stat_name, username))

            conn.commit()


    def purge_votes (self, gameid):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            sql = ('delete from vote where game_id=%s')
            cur.execute(sql, (gameid,))
            conn.commit()

    def check_werewolves (self, gameid):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            sql = ('select * from player where is_werewolf=1')
            cur.execute(sql, (gameid,))
            count = 0
            for row in cur.fetchall():
                count += 1
            
            return count 


    def purge_inventory (self, gameid):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            sql = ('delete from inventory using player where '
                    'inventory.playerid=player.player_id and '
                    'player.game_id=%s')
            cur.execute(sql, (gameid,))
            conn.commit()

    def purge_players (self, gameid):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            sql = ('update gameuser as g set current_player=NULL '
                    'from player as p where g.current_player=p.player_id '
                    'and p.game_id=%s')
            cur.execute(sql, (gameid,))
            conn.commit()

    def get_admin (self, gameid):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            sql = ('select username from gameuser, game where '
                    'gameuser.user_id=game.admin_id and '
                    'game.game_id=%s')
            cur.execute(sql, (gameid,))
            return (cur.fetchone()[0])

    def random_item (self):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            sql = ('select name from item order by random() limit 1')
            cur.execute(sql)
            return (cur.fetchone()[0])

    def add_landmark(self, lat, lng, radius, kind, gameid):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            sql = ('insert into landmark (lat, lng, radius, type, game_id, '
                    'created_at) values (%s, %s, %s, %s, %s, now())')
            cur.execute(sql, (lat, lng, radius, kind, gameid,))
            conn.commit()

    def add_treasure(self, itemname):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            sql = ('insert into treasure (landmark_id, item_id, quantity) '
                    'values ((select landmark_id from landmark order by '
                    'created_at desc limit 1), (select itemid from item '
                    'where name=%s ), 1)')
            cur.execute(sql, (itemname,))
            conn.commit()


    def add_treasure_admin(self, itemname, landmark_id):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            sql = ('insert into treasure (landmark_id, item_id, quantity) '
                    'values (%s, (select itemid from item '
                    'where name=%s ), 1)')
            cur.execute(sql, (landmark_id, itemname,))
            conn.commit()


    def get_landmarks_nearby(self, username, game_id, radius): 
        ''' returns all alive players near a player '''
        conn = self.get_db()
        result = []
        with conn:
            c = conn.cursor()
            # using the radius for lookups now
            sql = ('select landmark_id, type, lat, lng from landmark where '
                   'earth_box(ll_to_earth( '
                   '(select lat from player, gameuser '
                   'where player.player_id = gameuser.current_player '
                   'and gameuser.username=%s), '
                   '(select lng from player, gameuser '
                   'where player.player_id = gameuser.current_player '
                   'and gameuser.username=%s)), %s) '
                   '@> ll_to_earth(lat, lng) '
                   'and is_active = 1 '
                   'and game_id = %s'
            )

            c.execute(sql, (username, username, radius, game_id))
            for row in c.fetchall():
                d = {}
                d["landmark_id"] = row[0]
                d["type"] = row[1]
                d["lat"] = row[2]
                d["lng"] = row[3]
                result.append(d)
        return result


    def get_landmarks(self, gameid):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            sql = ('select landmark_id, lat, lng, type from landmark where game_id=%s')
            cur.execute(sql, (gameid,))

            results = []
            for row in cur.fetchall():
                landmark_id = row[0]
                lat = row[1]
                lng = row[2]
                kind = row[3]
                temp = {'landmark_id': landmark_id, 'lat': lat, 'lng':lng}
                results.append(temp)

            return results

    def get_treasure (self, landmark_id):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            sql = ('select item_id, quantity  from treasure where landmark_id=%s')        
            cur.execute(sql, (landmark_id,))
            for row in cur.fetchall():
                item_id = row[0]
                quantity = row[1]

            return {'item_id': item_id, 'quantity': quantity}            

    def obtain_treasure(self, username, itemid):
        conn = self.get_db()
        with conn:
            cur=conn.cursor()

            cmdupdate = ('update inventory set quantity=quantity+1'
                         'where itemid=%s' 
                         'and playerid='
                         '(select current_player from gameuser where username=%s);')
            cmd = ('insert into inventory (playerid, itemid, quantity)' 
                   'select (select current_player from gameuser where username=%s) as cplayer,'
                   '%s as item,' 
                   '1 where not exists' 
                   '(select 1 from inventory where itemid=%s' 
                   'and playerid=(select current_player from gameuser where username=%s))')
            cur.execute(cmdupdate + cmd, (itemid, username, username, itemid, itemid, username))

            conn.commit()


    def delete_treasure (self, landmark_id):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            sql = ('delete from treasure where landmark_id=%s')
            cur.execute(sql, (landmark_id,))

            sql2 = ('update landmark set is_active=0 where landmark_id=%s')        
            cur.execute(sql2, (landmark_id,))

            conn.commit()          


    def pending_achievements (self, gameid):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            sql = ('select gameuser.username, achievement.name from '
                    'player_stat, achievement, gameuser, player where '
                    'player_stat.stat_name=achievement.name and '
                    'player_stat.player_id=gameuser.current_player and '
                    'gameuser.current_player=player.player_id and player.game_id=%s')
            cur.execute(sql, (gameid,))
            achievements = []
            for row in cur.fetchall():
                d = {}
                d["username"] = row[0]
                d["achievement_name"] = row[1]
                achievements.append(d)

            return achievements

    def pending_stats (self, gameid):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            sql = ('select gameuser.username, player_stat.stat_name, '
                    'player_stat.stat_value from player_stat, gameuser, '
                    'player where player_stat.stat_name!=%s and '
                    'player_stat.player_id=gameuser.current_player and '
                    'gameuser.current_player=player.player_id and player.game_id=%s')
            cur.execute(sql, ('cooldown', gameid,))
            achievements = []
            for row in cur.fetchall():
                d = {}
                d["username"] = row[0]
                d["stat_name"] = row[1] 
                d["stat_value"] = row[2]
                achievements.append(d)

            return achievements        


    def in_cooldown (self, username):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            sql = ('select (now()-player_stat.created_at) from player_stat, '
                    'gameuser where player_stat.stat_name=%s and '
                    'player_stat.player_id = gameuser.current_player and '
                    'gameuser.username=%s')
            cur.execute(sql, ('cooldown', username,))
            duration = cur.fetchone()

            if duration >= ('00:30:00.000000'):
                sql2 = ('delete from player_stat using gameuser where '
                        'player_stat.stat_name=%s and '
                        'player_stat.player_id=gameuser.current_player and '
                        'gameuser.username=%s')
                cur.execute(sql, ('cooldown', username,))
                conn.commit()

            return duration >= ('00:30:00.000000')

    def gameover(self, gameid):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            sql = ('select count (*) from player where is_werewolf=0 and is_dead=0 and game_id=%s')
            cur.execute(sql, (gameid,))
            remaining_villagers = cur.fetchone()[0]

            sql2 = ('select count (*) from player where is_werewolf=1 and is_dead=0 and game_id=%s')
            cur.execute(sql2, (gameid,))
            remaining_werewolves = cur.fetchone()[0]

            if remaining_villagers == 0:
                sql3 = ('select username from player, gameuser where '
                        'player.player_id=gameuser.current_player and '
                        'player.is_werewolf=1 and player.is_dead=0 and '
                        'player.game_id=%s')
                cur.execute(sql3, (gameid,))
                for row in cur.fetchall():
                    username = row[0]
                    self.give_player_stat(username, 'win')

                sql4 = ('select username from player, gameuser where '
                        'player.player_id=gameuser.current_player and '
                        'player.is_werewolf=0 and player.is_dead=1 and '
                        'player.game_id=%s')
                cur.execute(sql4, (gameid,))
                for row in cur.fetchall():
                    username = row[0]
                    self.give_player_stat(username, 'loss')

                return True


            elif remaining_werewolves == 0:
                sql3 = ('select username from player, gameuser where '
                        'player.player_id=gameuser.current_player and '
                        'player.is_werewolf=0 and player.is_dead=0 and '
                        'player.game_id=%s')
                cur.execute(sql3, (gameid,))
                for row in cur.fetchall():
                    username = row[0]
                    self.give_player_stat(username, 'win')

                sql4 = ('select username from player, gameuser where '
                        'player.player_id=gameuser.current_player and '
                        'player.is_werewolf=1 and player.is_dead=1 and '
                        'player.game_id=%s')
                cur.execute(sql4, (gameid,))
                for row in cur.fetchall():
                    username = row[0]
                    self.give_player_stat(username, 'loss')

                return True

            else:
                return False



    def reset(self, gameid):
        conn = self.get_db()
        with conn:
            cur = conn.cursor()
            sql_inventory = ('delete from inventory using player where '
                    'inventory.playerid=player.player_id and player.game_id=%s')
            cur.execute(sql_inventory, (gameid,))

            sql_player_stat = ('delete from player_stat using player where '
                    'player_stat.player_id=player.player_id and player.game_id=%s')
            cur.execute(sql_player_stat, (gameid,))

            sql_treasure = ('delete from treasure using landmark where '
                    'treasure.landmark_id=landmark.landmark_id and landmark.game_id=%s')
            cur.execute(sql_treasure, (gameid,))

            sql_vote = ('delete from vote where game_id=%s')
            cur.execute(sql_vote, (gameid,))

            sql_landmark = ('delete from landmark where game_id=%s')
            cur.execute(sql_landmark, (gameid,))

            sql_gameuser = ('update gameuser set current_player=NULL '
                    'from player where gameuser.current_player=player.player_id '
                    'and player.game_id=%s')
            cur.execute(sql_gameuser, (gameid,))

            sql_player = ('delete from player where game_id=%s')
            cur.execute(sql_player, (gameid,))

            sql_game = ('delete from game where game_id=%s')
            cur.execute(sql_game, (gameid,))

            conn.commit()



if __name__ == "__main__":
    pass

    # dao = WherewolfDao('wherewolf','postgres','')

    # # dao.clear_tables()
    # # try:
    # #     dao.create_user('rfdickerson', 'awesome', 'Robert', 'Dickerson')
    # #     dao.create_user('oliver','furry','Oliver','Cat')
    # #     dao.create_user('vanhelsing', 'van', 'Van', 'Helsing')
    # #     print 'Created a new player!'
    # # except UserAlreadyExistsException as e:
    # #     print e
    # # except Exception:
    # #     print 'General error happened'
        
    # # username = 'rfdickerson'
    # # correct_pass = 'awesome'
    # # incorrect_pass = 'scaley'
    # # print 'Logging in {} with {}'.format(username, correct_pass)
    # # print 'Result: {} '.format( dao.check_password(username, correct_pass ))
    
    # # print 'Logging in {} with {}'.format(username, incorrect_pass)
    # # print 'Result: {} '.format( dao.check_password(username, incorrect_pass ))

    # # game_id = dao.create_game('rfdickerson', 'TheGame')
    # # # dao.create_game('oliver', 'AnotherGame')
    
    # # dao.join_game('oliver', game_id)
    # # dao.join_game('rfdickerson', game_id)
    # # dao.join_game('vanhelsing', game_id)

    # # print "Adding some items..."
    # # dao.add_item('rfdickerson', 'Silver Knife')
    # # dao.add_item('rfdickerson', 'Blunderbuss')
    # # dao.add_item('rfdickerson', 'Blunderbuss')
    # # dao.add_item('rfdickerson', 'Blunderbuss')
    # # dao.add_item('oliver', 'Blunderbuss')
    # # dao.remove_item('rfdickerson', 'Blunderbuss')

    # # print
    # # print 'rfdickerson items'
    # # print '--------------------------------'
    # # items = dao.get_items("rfdickerson")
    # # for item in items:
    # #     print item["name"] + "\t" + str(item["quantity"])
    # # print

    # # # location stuff
    # # dao.set_location('rfdickerson', 30.25, 97.75)
    # # dao.set_location('oliver', 30.3, 97.76)
    # # dao.set_location('vanhelsing', 30.2, 97.7) 
    # # loc = dao.get_location('rfdickerson')
    # # loc2 = dao.get_location('oliver')
    # # print "rfdickerson at {}, {}".format(loc["lat"], loc["lng"]) 
    # # print "oliver at {}, {}".format(loc2["lat"], loc2["lng"]) 

    # # dao.award_achievement('rfdickerson', 'Children of the moon')
    # # dao.award_achievement('rfdickerson', 'A hairy situation')
    # # achievements = dao.get_achievements("rfdickerson")

    # # print
    # # print 'rfdickerson\'s achievements'
    # # print '--------------------------------'
    # # for a in achievements:
    # #     print "{} ({}) - {}".format(a["name"],a["description"],a["created_at"].strftime('%a, %H:%M'))
    # # print
    
    # # nearby = dao.get_alive_nearby('rfdickerson', game_id, 20)
    # # print ('Nearby players: ')
    # # for p in nearby:
    # #     print "{}".format(p["player_id"])

    # # dao.vote(game_id, 'rfdickerson', 'oliver')
    # # dao.vote(game_id, 'oliver', 'vanhelsing')
    # # dao.vote(game_id, 'vanhelsing', 'oliver')
    # # # print 'Players in game 1 are'
    # # # print dao.get_players(1)
    
    # # dao.set_dead('rfdickerson')
