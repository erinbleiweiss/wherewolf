from flask import Flask, request, jsonify
app = Flask(__name__)

from wherewolfdao import WherewolfDao, UserAlreadyExistsException, NoUserExistsException, BadArgumentsException
from math import radians, sin, cos, sqrt, asin, atan2, ceil, pi
import time
from datetime import datetime
import random

DAYBREAKHOUR = 7      # 7:00 am
NIGHTFALLHOUR = 19    # 7:00 pm

@app.route('/healthcheck')
def health_check():
		return "healthy"

def checkIsNight():
	hour = int(datetime.now().strftime('%H'))
	hour = 10
	day = DAYBREAKHOUR       # 7:00 am
	night = NIGHTFALLHOUR    # 7:00 pm

	return ((hour >= night) or (hour < day))


def haversine(lat1, lon1, lat2, lon2):
 
	R = 6372.8 * 1000 # Earth radius in kilometers
 
	dLat = radians(lat2 - lat1)
	dLon = radians(lon2 - lon1)
	lat1 = radians(lat1)
	lat2 = radians(lat2)
 
	a = sin(dLat/2)**2 + cos(lat1)*cos(lat2)*sin(dLon/2)**2
	c = 2*asin(sqrt(a))
 
	return R * c


def checkNotInGame(gameid, username):
	db = WherewolfDao()

	notInGame = True
	allplayers = db.get_players(gameid)
	for player in allplayers:
		playerid = player["playerid"]
		p_username = db.get_username(playerid)
		if p_username == username:
			notInGame = False 

	return notInGame


def checkIsAdmin(gameid, username):
	db = WherewolfDao()

	allgameinfo = db.game_info(gameid)
	userid = db.get_userid(username)

	return allgameinfo["admin_id"] == userid

def getArmor(username):
	db = WherewolfDao()

	armor = 0

	alliteminfo = db.get_items(username)
	for item in alliteminfo:
		if item["armor"]:
			armor += (item["armor"] * item["quantity"])

	return armor


def getDamage(username):
	db = WherewolfDao()

	damage = 0

	alliteminfo = db.get_items(username)

	for item in alliteminfo:
		if item["damage"]:
			damage += (item["damage"] * item["quantity"])

	return damage


def getRank(username, gameid):
	db = WherewolfDao()

	allplayers = db.get_players(gameid)
	for player in allplayers:
		user = db.get_username(player["playerid"])
		if user == username:
			return player["is_werewolf"]


def checkGameOver(gameid):
	db = WherewolfDao()

	try:
		return (db.gameover(gameid))
	except Exception:
		return False


def checkNearLandmark(username, gameid):
	db = WherewolfDao()

	location = db.get_location(username)  
	lat = location["lat"]
	lng = location["lng"]

	try:
		lat = float(lat.encode('utf-8'))
		lng = float(lng.encode('utf-8'))
	except Exception:
		lat = float(lat)
		lng = float(lng)

	nearby = []

	for i in db.get_landmarks(gameid):
		p_lat = i["lat"]
		p_lng = i["lng"]

		#  Distance btw lat/lng pairs
		R = 6373.0
		dlng = lng - p_lng
		dlat = lat - p_lat
		a = (sin(dlat/2))**2 + cos(p_lat) * cos(lat) * (sin(dlng/2))**2
		c = 2 * atan2(sqrt(a), sqrt(1-a))
		distance = R * c

		if distance <= .5:
			nearby.append(i["landmark_id"])

	return nearby


def checkisSafezone (gameid, landmark_id):
	db = WherewolfDao()

	for i in db.get_landmarks(gameid):
		if i["landmark_id"] == landmark_id:
			return (i["kind"] == 0)


def nightfall(gameid):
	db = WherewolfDao()
	response = {}

	try:
		tally = db.count_votes(gameid)
	except Exception:
		response["count"] = ("\nVotes could not be counted.") 

	majority = 0
	evicted = 0

	for i in tally:
		if i["votes"] > majority:
			majority = i["votes"]
			evicted = i["playerid"]

	evicted_player = db.get_username(evicted)
	try:
		db.set_dead(evicted_player)
		response["evicted"] = ("Success - Player \"{}\" has been voted out.").format(evicted_player)
	except NoUserExistsException:
		response["evicted"] = ("Failure - Unable to evict player \"{}\".").format(evicted_player)
	except Exception:
		response["evicted"] = ("Failure - Unable to evict player \"{}\".").format(evicted_player)

	isVillager = True
	if getRank(evicted_player, gameid) != 0:
		isVillager = False

	if isVillager:
		players = db.vote_by_target(gameid, evicted)

		for i in players:
			try:
				db.give_player_stat(i, 'It is never Lupus')
				response["achievement " + str(i)] = ('Success - Awarded player \"{}\" stat \"It is never Lupus\".').format(i)
			except Exception:
				response["achievement " + str(i)] = ('Failure - Could not award player \"{}\" stat \"It is never Lupus\".').format(i)

	try:
		db.purge_votes(gameid)
		response["purge"] = ('Success - Purged votes from game \"{}\".').format(gameid)
	except Exception:
		response["purge"] = ('Failure - Could not purge votes from game \"{}\".').format(gameid)



	try:
		numWolves = db.check_werewolves(gameid)
		if numWolves == 0:
			end_game(gameid)
	except Exception:
		response["checkwolves"] = ('\nFailure - Could not check for werewolves remaining in game.')

	return response

def random_point(lat, lng):
	# At latitude 30 degrees (UT Austin), 1 degree = 110852.4248 meters
	# Radius = about 500m
	RADIUS = 0.004510501

	# random lat/lng:
	u = random.random()
	v = random.random()

	w = RADIUS * sqrt(u)
	t = 2 * pi * v
	x = w * cos(t)
	y = w * sin(t)

	newlat = lat + x
	newlng = lng + y

	x = x / cos(lng)

	return {'lat': newlat, 'lng': newlng}


def spawn_treasure(gameid, n):
	db = WherewolfDao()

	RADIUS = 50

	response = {}


	try:
		admin = db.get_admin(gameid)
	except Exception:
		response["admin_treasure"] = ('Failure - Could not obtain admin ID for game {}.').format(gameid)

	try:
		origin = db.get_location(admin)

		lat = origin["lat"]
		lng = origin["lng"]

		for i in range(1, n+1):
			# random location:
			point = random_point(lat, lng)
			newlat = point["lat"]
			newlng = point["lng"]

			try:
				db.add_landmark(newlat, newlng, RADIUS, 1, gameid)
				response["created_treasure " + str(i)] = ('Success - Spawned new treasure hoard at \"{}, {}.\"').format(newlat, newlng)
			except Exception:
				response["created_treasure " + str(i)] = ('Failure - Unable to spawn new treasure hoard')

			item = db.random_item()
			try:
				db.add_treasure(item)
				response["treasure " + str(i)] = ('Success - Added item \"{}\" to treasure hoard {}.').format(item, i)
			except Exception:
				response["treasure " + str(i)] = ('Failure - Unable to add item \"{}\" to treasure hoard {}.').format(item, i)



	except Exception:
		response["admin_treasure"] = ('Failure - Could not obtain admin location for game {}.').format(gameid)






	return response



def spawn_safezone(gameid, n):
	db = WherewolfDao()

	RADIUS = 50

	response = {}

	try:
		admin = db.get_admin(gameid)
	except Exception:
		response["admin_safe"] = ('Failure - Could not obtain admin ID for game {}.').format(gameid)

	try:
		origin = db.get_location(admin)

		lat = origin["lat"]
		lng = origin["lng"]


		for i in range(0, n):
			# random location:
			point = random_point(lat, lng)
			newlat = point["lat"]
			newlng = point["lng"]

			try:
				db.add_landmark(newlat, newlng, RADIUS, 0, gameid)
				response["created safe " + str(i)] = ('Success - Spawned new safezone at \"{}, {}.\"').format(newlat, newlng)
			except Exception:
				response["created safe " + str(i)] = ('Failure - Unable to spawn new safezone')



	except Exception:
		response["admin_safe"] = ('Failure - Could not obtain admin location for game {}.').format(gameid)



	return response


def end_game(gameid):
	db = WherewolfDao()

	response = {}
	achievements_response = award_achievements(gameid)


	try:
		db.reset(gameid)
		response["status"] = "success"
		response["message"] = (' - Game {} data cleared.').format(gameid)
	except Exception:
		response["status"] = "failure"
		response["error"] = (' - Could not clear data for game {}').format(gameid)

	dicts = [response, achievements_response]

	newresponse = {}
	for d in dicts:
			for k, v in d.iteritems():
					newresponse.setdefault(k, []).append(v)

	return newresponse



def award_achievements(gameid):
	db = WherewolfDao()

	response = {}

	try:
		pending_achievements = db.pending_achievements(gameid)
		if pending_achievements != [{}]:
			for i in pending_achievements:
				username = i["username"]
				achievement_name = i["achievement_name"]
				db.award_achievement(username, achievement_name)
				response["status"] = "success"
				response["message"] = ("Awarded user \"{}\" achievement \"{}\".").format(username, achievement_name)
	except Exception:
		response["status"] = "failure"
		response["error"] = ("Could not award achievements.")


	try:
		pending_stats = db.pending_stats(gameid)
		if pending_stats != [{}]:
			for i in pending_stats:
				username = i["username"]
				stat_name = i["stat_name"]
				stat_value = i["stat_value"]

				#Format:
				if stat_name == 'kill':
					stat_name = 'Kills'
				elif stat_name == 'win':
					stat_name = 'Wins'
				elif stat_name == 'loss':
					stat_name = 'Losses'

				db.award_stat(username, stat_name, stat_value)
				response["status"] = "success"
				response["message"] = ("Awarded user \"{}\" {} \"{}\".").format(username, stat_value, stat_name)
	except Exception:
			response["status"] = "failure"
			response["error"] = ("Could not award user stats")
	return response


@app.route("/")
def hello():
	return "hello world"

@app.route("/v1/check_password", methods=["GET"])
def check_password():
	db = WherewolfDao()
	auth = request.authorization

	username = auth.username
	password = auth.password 

	response = {}

	if db.check_password(username, password):
		response["status"] = "success"
	else:
		response["status"] = "failure"

	return jsonify(response)


@app.route('/v1/register', methods=["POST"])
def create_user():
	username = request.form['username']
	password = request.form['password']
	firstname = request.form['firstname']
	lastname = request.form['lastname']
	avatar = request.form['avatar']

	print avatar

	db = WherewolfDao()

	response = {}

	if len(password) >= 6:
		try:
			db.create_user(username, password, firstname, lastname, avatar)
			response["status"] = "success"
			response["message"] = ("Created user \"{}\".").format(username)
		except UserAlreadyExistsException:
			response["status"] = "failure"
			response["error"] = ("User \"{}\" already exists.").format(username)
		except Exception:
			response["status"] = "failure"
			response["error"] = "generic error happened"
	else:
		response["status"] = "failure"
		response["error"] = "Invalid password length."

	print response["status"]
	try:
		print response["message"]
	except Exception:
		print response["error"]

	return jsonify(response)


@app.route('/v1/game', methods=["POST"])
def create_game():
	db = WherewolfDao()

	auth = request.authorization

	username = auth.username
	password = auth.password 

	game_name = request.form['game_name']
	description = request.form['description']

	response = {}

	badAuth = False
	# check authorization
	if not check_password():
		response["status"] = "failure"
		response["error"] = ("Bad authorization for user \"{}\".").format(username)
		badAuth = True

	#get userid
	userid = db.get_userid(username)

	alreadyInGame = False
	# check user already administering a game
	allgames = db.get_games()
	for g in allgames:
		all_game_id = g["game_id"]
		allgameinfo = db.game_info(all_game_id)
		if allgameinfo["admin_id"] == userid:
			response["status"] = "failure"
			response["error"] = ("User \"{}\" already administering game.").format(username)
			alreadyInGame = True


	if not alreadyInGame and not badAuth:
		try:
			game_id = db.create_game(username, game_name, description) 

			response["status"] = "success"
			response["message"] = ("Created game \"{}\" (id: {}) with admin \"{}\"").format(game_name, game_id, username)
			response["results"] = {'game_id': game_id}
		except BadArgumentsException:
			response["status"] = "failure"
			response["results"] = {'game_id': None}

		try:
			db.join_game(username, game_id)
			response["status"] = "success"
			response["message"] = ("Added user \"{}\" to game.").format(username)
		except Exception:
			response["status"] = "failure"
			response["error"] = ("Unable to add \"{}\" to game.").format(username)

	elif badAuth:
			response["status"] = "failure"
			response["error"] = ("Bad authorization for user \"{}\".").format(username)
			response["results"] = {'game_id': None}

	elif alreadyInGame:
			response["status"] = "failure"
			response["error"] = ("User \"{}\" is already in a game.").format(username)
			response["results"] = {'game_id': None}

	return jsonify(response)


@app.route('/v1/game/<gameid>', methods=["DELETE"])
def leave_game(gameid):
	db = WherewolfDao()
	auth = request.authorization
	username = auth.username
	password = auth.password 

	response = {}

	badAuth = False
	# check authorization
	if not check_password():
		response["status"] = "failure"
		response["error"] = ("Bad authorization for user \"{}\".").format(username)
		badAuth = True

	isNotAdmin = False
	# check if user is admin
	if not checkIsAdmin(gameid, username):
		response["status"] = "failure"
		response["error"] = ("User \"{}\" is not the administrator of game {}.").format(username, gameid)
		isNotAdmin = True

	if not badAuth and not isNotAdmin:
		try:
			db.reset(gameid)
			response["status"] = "success"
			response["message"] = ("Game \"{}\" has been deleted.").format(gameid)
		except Exception:
			response["status"] = "failure"
			response["error"] = ("Game \"{}\" could not be deleted.").format(gameid)

	return jsonify(response)


@app.route('/v1/game/<gameid>/lobby', methods=["POST"])
def join_game(gameid):
	db = WherewolfDao()
	auth = request.authorization
	username = auth.username
	password = auth.password 

	response = {}

	badAuth = False
	# check authorization
	if not check_password():
		response["status"] = "failure"
		response["error"] = "Bad authorization."
		badAuth = True


	# isAdmin = False`
	# # check is admin
	# if db.get_gameid(username) != None:
	#   isAdmin = True
	#   response["status"] = ("Failure - User {} is already administering a game.").format(username)


	inGame = False
	# check is already in game
	if db.get_gameid(username) != None:
		inGame = True
		response["status"] = "failure"
		response["error"] = ("User {} is already in a game.").format(username)


	inLobby = True
	#check game in lobby mode
	allgameinfo = db.game_info(gameid)
	if allgameinfo["game_id"] == gameid and game["status"] != 0:
		inLobby = False
		response["status"] = "failure"
		response["error"] = "Game is not in lobby mode."

	if not badAuth and not inGame and inLobby:
		try:
			db.join_game(username, gameid)
			response["status"] = "success"
			response["message"] = ("User {} has joined game id {}.").format(username, gameid)
		except NoUserExistsException:
			response["status"] = "failure"
			response["error"] = ("User: {} does not exist").format(username)
		except BadArgumentsException:
			response["status"] = "failure"
			response["error"] = ("Bad arguments user: {}").format(username)
		except Exception:
			response["status"] = "failure"
			response["error"] = ("Other exception user: {}").format(username)
	else:
		response["status"] = "failure"
		response["error"] = ("User: {} could not join game.").format(username)

	return jsonify(response)


@app.route('/v1/game/<gameid>/lobby', methods=["DELETE"])
def leave_game_lobby(gameid):
	db = WherewolfDao()
	auth = request.authorization
	username = auth.username
	password = auth.password 

	response = {}

	badAuth = False
	# check authorization
	if not check_password():
		response["status"] = "failure"
		response["error"] = "Bad authorization."
		badAuth = True

	isAdmin = checkIsAdmin(gameid, username)

	if isAdmin:
		return leave_game(gameid)
	else:
		db.leave_game_lobby(username)
		response["status"] = "success"
		response["message"] = "left game lobby"

	return jsonify(response)




@app.route('/v1/games', methods=["GET"])
def get_games():
	db = WherewolfDao()
	auth = request.authorization
	username = auth.username
	password = auth.password 

	response = {} 

	try:
		allgames = db.get_games()
		response["games"] = []
		for i in allgames:
			row = {}
			row["game_id"] = i["game_id"]
			row["name"] = i["name"]
			row["admin_name"] = i["admin_name"]
			response["games"].append(row)

		response["status"] = "success"
		response["message"] = "retrieved games"
	except Exception:
		response["status"] = "failure"
		response["error"] = "could not retrieve games"

	return jsonify(response)


@app.route('/v1/game/<gameid>/players', methods=["GET"])
def get_players(gameid):
	db = WherewolfDao()
	auth = request.authorization
	username = auth.username
	password = auth.password 

	response = {}
	try:
		allplayers = db.get_player_names(gameid)
		response["players"] = []
		for i in allplayers:
			row = {}
			row["playername"] = i["playername"]
			row["playerid"] = i["playerid"]
			row["avatar"] = i["avatar"]
			response["players"].append(row)

		response["status"] = "success"
		response["message"] = "retrieved players"
	except Exception:
		response["status"] = "failure"
		response["error"] = "could not retrieve players"

	return jsonify(response)


@app.route('/v1/game/<gameid>/start', methods=["POST"])
def start_game(gameid):
	db = WherewolfDao()
	auth = request.authorization
	username = auth.username
	password = auth.password 

	response = {}

	# check authorization
	badAuth = False
	if not check_password():
		response["status"] = "failure"
		response["error"] = ("Bad authorization for user \"{}\".").format(username)
		badAuth = True

	isNotAdmin = False
	# check if user is admin
	if not checkIsAdmin(gameid, username):
		response["status"] = "failure"
		response["error"] = ("User \"{}\" is not the administrator of game {}.").format(username, gameid)
		isNotAdmin = True


	if badAuth or isNotAdmin:
		return jsonify(response)
	else:
		response["status"] = "Success"

	# Set game status to "1"  
	try:
		db.set_game_status(gameid, 1)
		response["status"] = "success"
		response["message"] = ("Game {} set to status 1.").format(gameid)
	except Exception:
		response["status"] = "failure"
		response["error"] = ("Game {} could not be set to status 1.").format(gameid)

	# randomly make werewolf assignments
	player_info = db.get_players(gameid)
	num_players = len(player_info)

	allplayers = []
	for i in player_info:
		allplayers.append(i["playerid"])

	random.shuffle(allplayers)
	num_werewolves = int(ceil(num_players * .3))

	i = 1
	while i <= num_werewolves:
		try:
			db.set_werewolf(allplayers[i], 1)
			user = db.get_username(allplayers[i])
			response["status"] = "success"
			response["message"] = ("User \"{}\" is a werewolf.").format(user)
		except Exception:
			response["status"] = "failure"
			response["error"] = ("Could not set user \"{}\" to werewolf.").format(user)
		i += 1

	# Spawn treasure hoards (num = num_werewolves)
	treasure_response = spawn_treasure(gameid, num_werewolves)
	safe_response = spawn_safezone(gameid, num_werewolves)

	dicts = [response, treasure_response, safe_response]

	newresponse = {}
	for d in dicts:
			for k, v in d.iteritems():
					newresponse.setdefault(k, []).append(v)

	return jsonify(newresponse)



@app.route('/v1/game/<gameid>', methods=["PUT"])
def update_game(gameid):
	db = WherewolfDao()
	auth = request.authorization
	username = auth.username
	password = auth.password 

	response = {}

	lat = request.form["lat"]
	lng = request.form["lng"]

	lat = float(lat.encode('utf-8'))
	lng = float(lng.encode('utf-8'))

	# update position
	db.set_location(username, lat, lng)

	# get alive nearby
	RADIUS = 50
	all_nearby = db.get_alive_nearby(username, gameid, RADIUS)

	nearby = []
	for player in all_nearby:
		player_id = player["player_id"]
		p_username = db.get_username(player_id)
		if p_username != username:
			p_location = db.get_location(p_username)
			p_lat = p_location["lat"]
			p_lng = p_location["lng"]

			distance = haversine(lat, lng, p_lat, p_lng)

			nearby.append({'player_id':player_id, 'distance':distance})



	nearby_landmarks = db.get_landmarks_nearby(username, gameid, RADIUS)
	if nearby_landmarks != []:
		for i in nearby_landmarks:
			if i["type"] == 1:
				try:
					treasure = db.get_treasure(i["landmark_id"])
					t_lat = i["lat"]
					t_lng = i["lng"]
					t_distance = haversine(lat, lng, t_lat, t_lng)

					db.obtain_treasure(username, treasure["item_id"])
					db.delete_treasure(i["landmark_id"])
					item_id = treasure["item_id"]
					response["treasure"] = ('Distance from treasure {}: {} \nUser \"{}\" obtained item id {} from treasure hoard {}').format(i["landmark_id"], t_distance, username, item_id, i)
				except Exception:
					response["treasure"] = ('Unable to award treasure to user \"{}\"').format(username)
			elif i["type"] == 0:
				pass

	# check for gameover
	if checkGameOver(gameid) == True:
		game_response = end_game(gameid)
	else:
		game_response = {}

	response["status"] = "success"
	response["message"] = "Updated game"

	response["results"] = ("Nearby players: {}").format({username:nearby})

	dicts = [response, game_response]

	newresponse = {}
	for d in dicts:
		for k, v in d.iteritems():
			newresponse.setdefault(k, []).append(v)

	print newresponse

	return jsonify(newresponse)


@app.route('/v1/game/<gameid>/info', methods=["POST"])
def update_game_info(gameid):
	db = WherewolfDao()
	auth = request.authorization
	username = auth.username
	password = auth.password 

	lat = request.form["lat"]
	lng = request.form["lng"]

	lat = float(lat.encode('utf-8'))
	lng = float(lng.encode('utf-8'))

	response = {}

	try:
		db.set_location(username, lat, lng)
		response["status"] = "success"
		response["message"] = "updated position"
	except Exception:
		response["status"] = "failure"
		response["error"] = "could not update position"

	response["current_time"] = time.time()

	print time.time()

	hour = int(datetime.now().strftime('%H'))
	minute = int(datetime.now().strftime('%m'))

	print str(hour) + ":" + str(minute)

	return jsonify(response)



@app.route('/v1/game/<gameid>', methods=["GET"])
def game_info(gameid):
	db = WherewolfDao()
	auth = request.authorization
	username = auth.username
	password = auth.password 

	response = {}
	response["gameid"] = ("Game id: {}").format(gameid)

	theplayers = {}
	# return players
	allplayers = db.get_players(gameid)
	for player in allplayers:
		playerid = player["playerid"]
		p_username = db.get_username(playerid)
		theplayers[playerid] = (p_username)

	response["players"] = theplayers

	# return time of day
	hour = datetime.now().strftime('%H')
	minute = datetime.now().strftime('%M')
	time = ("{}:{}").format(hour, minute)

	response["time"] = ("Time: {}").format(time)

	if checkIsNight():
		cycle = "Night"
	else:
		cycle = "Day"

	response["cycle"] = ("Cycle: {}").format(cycle)

	return jsonify(response)


@app.route('/v1/game/<gameid>/ballot', methods=["POST"])
def cast_vote(gameid):
	db = WherewolfDao()

	auth = request.authorization
	username = auth.username
	password = auth.password 

	target_username = request.form["target_username"]

	response = {}

	# check authorization
	badAuth = False
	if not check_password():
		response["status"] = "failure"
		response["error"] = ("Bad authorization for user \"{}\".").format(username)
		badAuth = True

	# check game is night
	if checkIsNight():
		response["status"] = "failure"
		response["error"] = ("User \"{}\" can't vote during night.").format(username)
		isNight = True
	else:
		isNight = False

	# check for user in game
	notInGame = checkNotInGame(gameid, username)
	if notInGame:
		response["status"] = "failure"
		response["error"] = ("User \"{}\" is not in the game.").format(username)


	if badAuth or isNight or notInGame:
		return jsonify(response)

	try:
		db.vote(gameid, username, target_username)
		response["status"] = "success"
		response["message"] = ("Player \"{}\" voted for player \"{}\".").format(username, target_username)
	except NoUserExistsException:
		response["status"] = "failure"
		response["error"] = ("User {} does not exist.").format(username)
	except Exception:
		response["status"] = "failure"
		response["error"] = ("Vote not cast by user \"{}\".").format(username)  

	return jsonify(response)


@app.route('/v1/game/<gameid>/ballot', methods=["GET"])
def count_votes(gameid):
	db = WherewolfDao()
	auth = request.authorization
	username = auth.username
	password = auth.password 

	tally = db.count_votes(gameid)

	response = {}

	# check authorization
	badAuth = False
	if not check_password():
		response["status"] = "failure"
		response["error"] = ("Bad authorization for user \"{}\".").format(username)
		badAuth = True

	# check for user in game
	notInGame = checkNotInGame(gameid, username)
	if notInGame:
		response["status"] = "failure"
		response["error"] = ("User \"{}\" is not in the game.").format(username)

	if badAuth or notInGame:
		return jsonify(response)

	try:
		db.count_votes(gameid)
		response["status"] = "success"
		response["results"] = tally
	except Exception:
		response["status"] = "failure"
		response["results"] = None

	return jsonify(response)


@app.route('/v1/game/<gameid>/attack', methods=["POST"])
def attack_villagers(gameid):
	db = WherewolfDao()
	auth = request.authorization
	username = auth.username
	password = auth.password 

	target_username = request.form["target_username"]

	try:
		location = db.get_location(username)
	except Exception:
		response["status"] = "failure"
		response["error"] = ("Could not get location for user \"{}\".").format(username)

	lat = location["lat"]
	lng = location["lng"]

	response = {}
	response["sequence"] = ""

	# check authorization
	badAuth = False
	if not check_password():
		response["status check 1"] = ("Failure - Bad authorization for user \"{}\".").format(username)
		badAuth = True

	# check for user in game
	notInGame = checkNotInGame(gameid, username)
	if notInGame:
		response["status check 2"] = ("Failure - User \"{}\" is not in the game.").format(username)

	# check player is villager 
	playerNotWerewolf = False
	if getRank(username, gameid) == 0:
		response["status check 3"] = ("Failure - User \"{}\" is not a werewolf.").format(username)
		playerNotWerewolf = True

	targetNotVillager = False
	if getRank(target_username, gameid) != 0:
		response["status check 4"] = ("Failure - Target user \"{}\" is not a villager.").format(target_username)
		targetNotVillager = True

	# check player is not werewolf in cooldown period
	inCooldown = False
	if db.in_cooldown(username):
		response["status check 5"] = ("Failure - User {} is in a cooldown period").format(username)
		inCooldown = True

	# check target/player is not in safezone
	inSafezone = False
	nearby = checkNearLandmark(username, gameid)
	for i in nearby:
		if checkisSafezone(gameid, i["landmark_id"]):
			response["status check 6"] = ("Failure - User user {} is in a safezone").username
			inSafezone = True

	targetInSafezone = False
	nearby2 = checkNearLandmark(target_username, gameid)
	for i in nearby2:
		try:
			if checkisSafezone(gameid, i["landmark_id"]):
				response["status check 7"] = ("Failure - Target user {} is in a safezone").target_username
				targetInSafezone = True
		except Exception:
			pass


	# check target player is nearby and alive 
	notAliveNearby = True
	try:
		nearby_players = db.get_alive_nearby(username, gameid, 50)
		for i in nearby_players:
			if i["player_id"] == db.get_playerid(target_username):
				notAliveNearby = False

		if notAliveNearby:
			response["status"] = "failure"
			response["error"] = ("Target user \"{}\" is not nearby user \"{}\".").format(target_username, username)
	except Exception:
		response["status"] = "failure"
		response["error"] = ("Could not check for nearby players.")



	if badAuth or notInGame or playerNotWerewolf or targetNotVillager or inCooldown or inSafezone or targetInSafezone or notAliveNearby:
		return jsonify(response)

	# get stats for players
	werewolfHP = (db.get_stats(username))["hp"] + getArmor(username)
	werewolfAttack = (db.get_stats(username))["attack"] + getDamage(username)

	villagerHP = (db.get_stats(target_username))["hp"] + getArmor(target_username)
	villagerAttack = (db.get_stats(target_username))["attack"] + getDamage(target_username)

	response["sequence"] += ('\nFIGHT BEGINS:')
	response["sequence"] += ('\nWerewolf - \"{}\"  HP: {}  Attack: {}').format(username, werewolfHP, werewolfAttack)
	response["sequence"] += ('\nVillager - \"{}\"  HP: {}  Attack: {}').format(target_username, villagerHP, villagerAttack)
	response["sequence"] += ('\n\n-------------------------')

	# The fight:
	theRound = 1
	while (werewolfHP > 0 and villagerHP > 0):
		response["sequence"] += ('\n\nRound {}: Werewolf Attack \n').format(theRound)

		wRoll = random.randint(1, werewolfAttack)
		villagerHP -= wRoll

		response["sequence"] += ('\n>> Werewolf attacks Villager for {} damage').format(wRoll)

		response["sequence"] += ('\nWerewolf - \"{}\"  HP: {}  Attack: {}').format(username, werewolfHP, werewolfAttack)
		response["sequence"] += ('\nVillager - \"{}\"  HP: {}  Attack: {}').format(target_username, villagerHP, villagerAttack)


		if villagerHP <= 0:
			response["status"] = ("Success - Player \"{}\" has defeated player \"{}\".").format(username, target_username)
			dead = target_username
			victor = username
			break


		response["sequence"] += ('\n\nRound {}: Villager Attack \n').format(theRound)
		vRoll = random.randint(1, villagerAttack)
		werewolfHP -= vRoll

		response["sequence"] += ('\n>> Villager attacks Werewolf for {} damage').format(vRoll)


		response["sequence"] += ('\nWerewolf - \"{}\"  HP: {}  Attack: {}').format(username, werewolfHP, werewolfAttack)
		response["sequence"] += ('\nVillager - \"{}\"  HP: {}  Attack: {}').format(target_username, villagerHP, villagerAttack)


		if werewolfHP <= 0:
			response["status"] = ("Success - Player \"{}\" has defeated player \"{}\".").format(target_username, username)
			dead = username
			victor = target_username
			break

		theRound += 1


	# set player to dead
	try:
		db.set_dead(dead)
		response["dead"] = ('Success - Player {} is dead.').format(dead)
	except Exception:
		response["dead"] = ('Failure - Unable to set player {} to dead.').format(dead)

	# increment numKills

	try:
		db.give_player_stat(victor, 'kill')
		response["kill"] = ('Success - Gave player {} 1 kill.').format(victor)
	except Exception:
		response["kill"] = ('Failure - Unable to give player {} a kill.').format(victor)

	# cooldown
	if victor == username:
		try:
			db.give_player_stat(victor, 'cooldown')
			response["cooldown"] = ('Success - Cooldown period initiated for \"{}\".').format(username)
		except Exception:
			response["cooldown"] = ('Failure - Could not initiate cooldown period for \"{}\".').format(username)


	return jsonify(response)


################ ADMINISTRATIVE (TESTING) FUNCTIONS: ################
@app.route('/v1/game/<gameid>/admin/time', methods=["POST"])
def set_game_time(gameid):
	db = WherewolfDao()
	response = {}

	hour = request.form["current_time"]
	day = DAYBREAKHOUR       # 7:00 am (7)
	night = NIGHTFALLHOUR    # 7:00 pm (19)

	hour = int(hour.encode('utf-8'))


	if ((hour >= 19) or (hour < 7)):
		# Don't tally votes on first night (or if there are no votes)
		if (db.count_votes(gameid)) == []:
			response["status"] = "success"
			response["message"] = "Set game to Night"
		else:
			nightfall_response = nightfall(gameid)
			response["status"] = "success"
			response["message"] = ' - Set game to Night'
	else:
			response["status"] = "success"
			response["message"] = "Set game to Day"


	try:
		dicts = [response, nightfall_response]
		newresponse = {}
		for d in dicts:
				for k, v in d.iteritems():
						newresponse.setdefault(k, []).append(v)
		return jsonify(newresponse)
	except Exception:
		return jsonify(response)

	


@app.route('/v1/game/<gameid>/admin/giveitem', methods=["POST"])
def give_item(gameid):
	db = WherewolfDao()

	user = request.form["username"]
	itemname = request.form["itemname"]

	response = {}

	try:
		db.add_item(user, itemname)
		response["status"] = ("Success - Gave user \"{}\" item \"{}\".").format(user, itemname)
	except NoUserExistsException:
		response["status"] = ("Failure - User {} does not exist.").format(user)
	except Exception:
		response["status"] = ("Failure - Could not give user \"{}\" item \"{}\".").format(user, itemname)

	return jsonify(response)


@app.route('/v1/game/<gameid>/admin/set_werewolf', methods=["POST"])
def set_werewolf(gameid):
	db = WherewolfDao()

	user = request.form["username"]
	state = request.form["state"]
	playerid = db.get_playerid(user)

	response = {}

	if state == 'werewolf':
		n_state = 1
	elif state == 'villager':
		n_state = 0
	else:
		response["status"] = ("Failure - invalid input for player state.")
		return jsonify(response) 


	try:
		db.set_werewolf(playerid, n_state)
		response["status"] = ("Success - User \"{}\" is a {}.").format(user, state)
	except NoUserExistsException:
		response["status"] = ("Failure - Could not set user \"{}\" to {}.").format(user, state)
	except Exception:
		response["status"] = ("Failure - Could not set user \"{}\" to {}.").format(user, state)

	return jsonify(response)


@app.route('/v1/game/<gameid>/admin/set_location', methods=["POST"])
def set_location(gameid):
	db = WherewolfDao()

	user = request.form["username"]
	lat = request.form["lat"]
	lng = request.form["lng"]

	response = {}

	try:
		db.set_location(user, lat, lng)
		response["status"] = ('Success - Updated player \"{}\" location to \"{}, {}\"').format(user, lat, lng)
	except NoUserExistsException:
		response["status"] = ('Failure - Could not update player \"{}\" location to \"{}, {}\"').format(user, lat, lng)
	except Exception:
		response["status"] = ('Failure - Could not update player \"{}\" location to \"{}, {}\"').format(user, lat, lng)

	return jsonify(response)


@app.route('/v1/game/<gameid>/admin/set_treasure', methods=["POST"])
def set_treasure(gameid):
	db = WherewolfDao()

	lat = request.form["lat"]
	lng = request.form["lng"]

	response = {}

	try:
		db.add_landmark(lat, lng, 50, 1, 1)
		response["status"] = ('Success - Added treasure hoard at ({}, {}).').format(lat, lng)
	except Exception:
		response["status"] = ('Failure - Could not add treasure hoard at ({}, {}).').format(lat, lng)

	return jsonify(response)



@app.route('/v1/game/<gameid>/admin/set_treasure_item', methods=["POST"])
def set_treasure_item(gameid):
	db = WherewolfDao()

	itemname = request.form["itemname"]
	landmark_id = request.form["landmark_id"]

	response = {}

	try:
		db.add_treasure_admin(itemname, landmark_id)
		response["status"] = ('Success - Added item \"{}\" to treasure id {}.').format(itemname, landmark_id)
	except Exception:
		response["status"] = ('Failure - Could not add item \"{}\" to treasure id {}.').format(itemname, landmark_id)

	return jsonify(response)


@app.route('/v1/game/<gameid>/admin/set_dead', methods=["POST"])
def set_dead(gameid):
	db = WherewolfDao()

	username = request.form["username"]
	response = {}

	try:
		db.set_dead(username)
		response["status"] = ('Success - Player {} is dead.').format(username)
	except Exception:
		response["status"] = ('Failure - Could not set player {} to dead.').format(username)

	return jsonify(response)


if __name__ == "__main__":
	app.run(host="0.0.0.0", debug=True)



















