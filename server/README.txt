Changes to wherewolfdao.py
---------------------------------------------
* Added get_userid() function
  - returns userid for a given username

* Added get_playerid() function
  - returns player_id for a given username

* Added get_username() function
	- returns username for a given playerid

* Added get_game_id function
 	- returns game_id for a given username

* Modified vote() function:
  - accepts usernames as parameter instead of ids

* Added count_votes() function:
	- returns vote tally for a given game, and player with most votes

* Added set_werewolf() function:
 - sets player "is_werewolf" status to 0 (villager) or 1-3 (werewolf with rank)

* Modified get_items() function:
  - returns info for armor and damage

* Added get_stats() function:
  - returns base hp and stats for a  given username

* Added vote_by_target() function:
  - returns list of users to voted for a target player

* Added give_player_stat() function:
  - temporary stats to award achievements 

* Added purge_votes() function

* Added check_werewolves() function
  - counts number of werewolves remaining in game

* Added purge_inventory() function

* Added purge_players() function

* Added random_item() function

* Added add_landmark() function

* Added add_treasure() function

* Added add_treasure_admin() function
 
* Added in_cooldown() function
  - checks for werewolf in cooldown period
  - deletes cooldown stat if cooldown expired

* Added award_stat() function
  - keeps track of num kills and games won/lost


Changes to game.py
---------------------------------------------
* Added helper functions:
 - checkIsNight()
 - checkNotInGame()
 - checkIsAdmin()
 - getArmor()
 - getDamage()
 - getRank()
 - nightfall()
 - end_game()



Changes to client.py
---------------------------------------------
* Modified print statements to be more informative
  - ex: in create_game() ...
  		changed: print response["status"]
  		to: print('Status: {} \nUser{} created game id {}.').format(status, username, gameid)

    OR
  - for i in response:
        print response[i]

* Added function set_game_status()

* game_info()
  - added [[ auth=(username, password) ]]

* cast_vote()
  - changed [[ game_id + "/vote" ]] to [[ str(game_id) + "/ballot" ]]
  - added [[ auth=(username, password) ]]
  - accepts usernames as inputs instead of player ids

* Added function count_votes(game_id)

* Added function start_game()
  - initializes game (sets status to 1)
  - randomly assigns werewolf/villager status
  - randomly assigns locations for treasure

* Added function attack_villagers()

* Changed arguments and API path for set_game_time()


Changes to schema.sql
---------------------------------------------
* Moved all "DROP TABLE" statements to the beginning of file
 
* Create table "attack_stat"

* Modified "item" to include damage (weapon) modifiers and armor rating

[[Insert Statments:]]
* Inserted base stats for werewolf, villager

* Inserted all items with relevant damage/armor ratings

* Changed "created_at" in landmark to "timestamp DEFAULT CURRENT_TIMESTAMP"

* In "player_stat" - changed "stat_value" to INTEGER
                   - added "created_at" 


* In "user_stat" - changed "stat_value" to INTEGER