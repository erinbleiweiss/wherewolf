import requests
import json

hostname = "http://wherewolf-loadbalancer-280752479.us-west-2.elb.amazonaws.com"
#hostname = "http://54.191.43.168:5000"
# user = 'rfdickerson'
# password = 'awesome'
game_id = 0

rest_prefix = "/v1"

''' Important functions
create a game
leave a game
update game state with location
cast a vote
'''

def create_user(username, password, firstname, lastname):
    payload = {'username': username, 'password': password, 'firstname': firstname, 'lastname': lastname}
    url = "{}{}{}".format(hostname, rest_prefix, "/register")
    r = requests.post(url, data=payload)

    response = r.json()
    print response["status"]

def create_game(username, password, game_name, description):
    payload = {'game_name': game_name, 'description': description}
    url = "{}{}{}".format(hostname, rest_prefix, "/game")
    # print 'sending {} to {}'.format(payload, url)
    r = requests.post(url, auth=(username, password), data=payload)

    response = r.json()
    #rjson = json.loads(response)
    #print rjson["status"]

    gameid = response["results"]["game_id"]
    status = response["status"]
    if status == "success":
        print('Status: {} \nUser {} created game id {}.').format(status, username, gameid)
    else:
        print status

    return gameid
    
def leave_game(username, password, game_name):
    r = requests.delete(hostname + rest_prefix + "/game/" + str(game_id), 
                        auth=(username, password))
    
    response = r.json()
    print response

def start_game(username, password, game_id):
    url = "{}{}/game/{}/start".format(hostname, rest_prefix, game_id)
    r = requests.post(url, auth=(username, password))

    response = r.json()

    for i in response:
        print response[i]


def update_game(username, password, game_id, lat, lng):
    """ reports to the game your current location, and the game 
    returns to you a list of players nearby """

    payload = {'lat': lat, 'lng': lng}
    url = "{}{}/game/{}".format(hostname, rest_prefix, game_id)
    r = requests.put(url, auth=(username, password), data=payload)
    response = r.json()

    print ("-----")
    print response

    for i in response:
        print response[i]


def game_info(username, password, game_id):
    ''' returns all the players, the time of day, and other options for the game '''
    r = requests.get(hostname + rest_prefix + "/game/" + str(game_id), auth=(username, password))
    response = r.json()
    for i in response:
        print response[i]

def cast_vote(username, password, game_id, target_username):
    payload = {'target_username': target_username}
    r = requests.post(hostname + rest_prefix + "/game/" + str(game_id) + "/ballot",  auth=(username, password), data=payload)

    response = r.json()

    print response["status"]


def count_votes(username, password, game_id):
    r = requests.get(hostname + rest_prefix + "/game/" + str(game_id) + "/ballot",  auth=(username, password))
    response = r.json()

    for i in response:
        print response[i]

def join_game(username, password, game_id):
    # print 'Joining game id {}'.format(game_id)
    payload = {'game_id': game_id}
    url = "{}{}/game/{}/lobby".format(hostname, rest_prefix, game_id)
    r = requests.post(url, auth=(username, password))
    response = r.json()

    print (response["status"])

def set_game_status(game_id, status):
    payload = {'status': status}
    url = "{}{}/game/{}".format(hostname, rest_prefix, game_id)
    r = requests.put(url, data=payload)

    response = r.json()
    print response["status"]

def get_games(username, password):
    r = requests.get(hostname + rest_prefix + "/game")
    r = r.json()
    return r["results"]

def attack_villagers(username, password, game_id, target_username):
    payload = {'target_username': target_username}
    r = requests.post(hostname + rest_prefix + "/game/" + str(game_id) + "/attack",  auth=(username, password), data=payload)
    response = r.json()

    for i in response:
        print response[i]



################ ADMINISTRATIVE (TESTING) FUNCTIONS: ################
def set_game_time(game_id, game_time):
    ''' allows you to override the current time to a user specified one'''
    payload = {'current_time': game_time}
    r = requests.post(hostname + rest_prefix + "/game/" + str(game_id) + "/admin/time", data=payload)
    response = r.json()

    for i in response:
        print response[i]

def give_item(game_id, username, itemname):
    payload = {'username': username, 'itemname': itemname}
    r = requests.post(hostname + rest_prefix + "/game/" + str(game_id) + "/admin/giveitem", data=payload)
    response = r.json()
    print response["status"]

def set_werewolf(game_id, username, state):
    payload = {'username': username, 'state': state}
    r = requests.post(hostname + rest_prefix + "/game/" + str(game_id) + "/admin/set_werewolf", data=payload)
    response = r.json()
    print response["status"]

def set_location(gameid, username, lat, lng):
    payload = {'username': username, 'lat': lat, 'lng': lng}
    r = requests.post(hostname + rest_prefix + "/game/" + str(game_id) + "/admin/set_location", data=payload)
    response = r.json()
    print response["status"]

def set_treasure(gameid, lat, lng):
    payload = {'lat': lat, 'lng': lng}
    r = requests.post(hostname + rest_prefix + "/game/" + str(game_id) + "/admin/set_treasure", data=payload)
    response = r.json()
    print response["status"]

def set_treasure_item(gameid, itemname, landmark_id):
    payload = {'itemname': itemname, 'landmark_id': landmark_id}
    r = requests.post(hostname + rest_prefix + "/game/" + str(game_id) + "/admin/set_treasure_item", data=payload)
    response = r.json()
    print response["status"]


def set_safezone(gameid, lat, lng):
    payload = {'lat': lat, 'lng': lng}
    r = requests.post(hostname + rest_prefix + "/game/" + str(game_id) + "/admin/set_safezone", data=payload)
    response = r.json()
    print response["status"]


def set_dead(gameid, username):
    payload = {'username': username}
    r = requests.post(hostname + rest_prefix + "/game/" + str(game_id) + "/admin/set_dead", data=payload)
    response = r.json()
    print response["status"]

def create_users():
    create_user('michael', 'paper1', 'Michael', 'Scott')
    create_user('dwight', 'paper1', 'Dwight', 'Schrute')
    create_user('jim', 'paper1', 'Jim', 'Halpert')
    create_user('pam', 'paper1', 'Pam', 'Beesly')
    create_user('ryan', 'paper1', 'Ryan', 'Howard')
    create_user('andy', 'paper1', 'Andy', 'Bernard')
    create_user('angela', 'paper1', 'Angela', 'Martin')
    create_user('toby', 'paper1', 'Toby', 'Flenderson')

    return

def werewolf_winning_game():
    game_id = create_game('michael', 'paper', 'NightHunt', 'A test for werewolf winning')
    games = get_games('michael', 'paper')
    for game in games:
        print "Id: {},\tName: {}".format(game["game_id"], game["name"])
    
    join_game('dwight', 'paper', game_id)
    join_game('jim', 'paper', game_id)
    join_game('pam', 'paper', game_id)
    join_game('ryan', 'paper', game_id)
    join_game('andy', 'paper', game_id)
    join_game('angela', 'paper', game_id)
    join_game('toby', 'paper', game_id)
    start_game('michael', 'paper', game_id)
    
    leave_game('michael', 'paper', game_id)
    

if __name__ == "__main__":
	print ('\n\n***************ADD CHARACTERS TO GAME***************')
	create_users()

	print ('\n\n***************CREATE GAME WITH ADMIN "michael"***************')
	gameid = create_game('michael', 'paper1', 'NightHunt', 'A Dunder Mifflin Game')

	print ('\n\n***************ADD OTHER USERS TO GAME***************')
	join_game('dwight', 'paper1', gameid)
	join_game('jim', 'paper1', gameid)
	join_game('pam', 'paper1', gameid)
	join_game('ryan', 'paper1', gameid)
	join_game('andy', 'paper1', gameid)
	join_game('angela', 'paper1', gameid)
	join_game('toby', 'paper1', gameid)

	print ('\n\n***************ADMIN OVERRIDE: MOVE ALL PLAYERS TO UT GDC***************')
	set_location(gameid, 'michael', 30.286554, -97.736627)
	set_location(gameid, 'dwight', 30.286554, -97.736627)
	set_location(gameid, 'jim', 30.286554, -97.736627)
	set_location(gameid, 'pam', 30.286554, -97.736627)
	set_location(gameid, 'ryan', 30.286554, -97.736627)
	set_location(gameid, 'andy', 30.286554, -97.736627)
	set_location(gameid, 'angela', 30.286554, -97.736627)
	set_location(gameid, 'toby', 30.286554, -97.736627)


	print ('\n\n***************START GAME***************')
	print ('  >>> Sets game status to "active" (1)')
	print ('  >>> Randomly sets 30%% of characters to werewolf')
	print ('  >>> Spawns number of treasure hoards equal to number of werewolves')
	print ('  >>> Randomly distributes one item per treasure hoard')
	print ('  >>> Spawns number of safe zones equal to number of werewolves')

	start_game('michael', 'paper1', gameid)

	print ('\n\n***************ADMIN OVERRIDE: MICHAEL IS A WEREWOLF, TOBY IS A VILLAGER***************')
	set_werewolf(gameid, 'michael', 'werewolf')
	set_werewolf(gameid, 'toby', 'villager')

	print ('\n\n***************GAME INFO***************')
	game_info('michael', 'paper1', gameid)


	print ('\n\n***************ADMIN OVERRIDE: SET ROUND TO NIGHT***************')
	set_game_time(gameid, 20)

	print ('\n\n***************ADMIN OVERRIDE: CREATE TREASURE HOARD AT UT TOWER***************')
	print ('***************ADMIN OVERRIDE: ADD \"SILVER PLATING\" TO TREASURE HOARD***************')
	set_treasure(gameid, 30.286190, -97.739346)
	set_treasure_item(gameid, 'Silver Plating', 7)

	print ('\n\n***************ADMIN OVERRIDE: MOVE TOBY TO NEAR TREASURE HOARD***************')
	set_location(gameid, 'toby', 30.286246, -97.739351)


	print ('\n\n***************UPDATE GAME (USER: TOBY)***************')
	update_game('toby', 'paper1', gameid, 30.286246, -97.739351)

	print ('\n\n***************ADMIN OVERRIDE: MOVE MICHAEL TO NEAR TOBY***************')
	set_location(gameid, 'michael', 30.286603, -97.739112)

	print ('\n\n***************UPDATE GAME (USER: MICHAEL)***************')
	update_game('michael', 'paper1', gameid, 30.286603, -97.739112)



	print ('\n***************MICHAEL ATTACKS TOBY***************')
	attack_villagers('michael', 'paper1', gameid, 'toby')	


	print ('\n\n***************MICHAEL TRIES TO ATTACK RYAN (who is not nearby)***************')
	attack_villagers('michael', 'paper1', gameid, 'ryan')	


	print ('\n***************ADMIN OVERRIDE: MOVE DWIGHT TO NEAR MICHAEL***************')
	set_location(gameid, 'dwight', 30.286246, -97.739351)

	print ('\n***************MICHAEL TRIES TO ATTACK DWIGHT (Michael is in a cooldown period)***************')
	attack_villagers('michael', 'paper1', gameid, 'dwight')	



	print ('\n***************VOTING***************')
	cast_vote('michael', 'paper1', gameid, 'toby')
	cast_vote('michael', 'paper1', gameid, 'dwight')
	cast_vote('pam', 'paper1', gameid, 'jim')
	cast_vote('jim', 'paper1', gameid, 'dwight')

	print ('\n>>Count votes:')
	count_votes('michael', 'paper1', gameid)


	print('\n>>Give Items')
	give_item(gameid, 'andy', 'Chain Mail')
	give_item(gameid, 'pam', 'Silver Dagger')


	print ('\n\n***************ADMIN OVERRIDE: SET ROUND TO Day***************')
	set_game_time(gameid, 7)

	print ('\n\n***************ADMIN OVERRIDE: SET ROUND TO Night***************')
	set_game_time(gameid, 20)


	print ('\n\n***************ADMIN OVERRIDE: SET EVERYONE (EXCEPT JIM) TO DEAD***************')
	set_dead(gameid, 'michael')
	set_dead(gameid, 'dwight')
	set_dead(gameid, 'pam')
	set_dead(gameid, 'ryan')
	set_dead(gameid, 'andy')
	set_dead(gameid, 'angela')
	set_dead(gameid, 'toby')


	print ('\n\n***************PlAYER MOVES TO TRIGGER GAME OVER***************')
	set_location(gameid, 'michael', 30.288688, -97.739757)
	update_game('michael', 'paper1', gameid, 30.288688, -97.739757)



