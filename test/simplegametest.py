import client

def main():
	print ('\n\n***************ADD CHARACTERS TO GAME***************')
	client.create_users()

	print ('\n\n***************CREATE GAME WITH ADMIN "michael"***************')
	gameid = client.create_game('michael', 'paper1', 'NightHunt', 'A Dunder Mifflin Game')

	print ('\n\n***************ADD OTHER USERS TO GAME***************')
	client.join_game('dwight', 'paper1', gameid)
	client.join_game('jim', 'paper1', gameid)
	client.join_game('pam', 'paper1', gameid)
	client.join_game('ryan', 'paper1', gameid)
	client.join_game('andy', 'paper1', gameid)
	client.join_game('angela', 'paper1', gameid)
	client.join_game('toby', 'paper1', gameid)

	print ('\n\n***************ADMIN OVERRIDE: MOVE ALL PLAYERS TO UT GDC***************')
	client.set_location(gameid, 'michael', 30.286554, -97.736627)
	client.set_location(gameid, 'dwight', 30.286554, -97.736627)
	client.set_location(gameid, 'jim', 30.286554, -97.736627)
	client.set_location(gameid, 'pam', 30.286554, -97.736627)
	client.set_location(gameid, 'ryan', 30.286554, -97.736627)
	client.set_location(gameid, 'andy', 30.286554, -97.736627)
	client.set_location(gameid, 'angela', 30.286554, -97.736627)
	client.set_location(gameid, 'toby', 30.286554, -97.736627)


	print ('\n\n***************START GAME***************')
	print ('  >>> Sets game status to "active" (1)')
	print ('  >>> Randomly sets 30%% of characters to werewolf')
	print ('  >>> Spawns number of treasure hoards equal to number of werewolves')
	print ('  >>> Randomly distributes one item per treasure hoard')
	print ('  >>> Spawns number of safe zones equal to number of werewolves')

	client.start_game('michael', 'paper1', gameid)

	print ('\n\n***************ADMIN OVERRIDE: MICHAEL IS A WEREWOLF, TOBY IS A VILLAGER***************')
	client.set_werewolf(gameid, 'michael', 'werewolf')
	client.set_werewolf(gameid, 'toby', 'villager')

	print ('\n\n***************GAME INFO***************')
	client.game_info('michael', 'paper1', gameid)


	print ('\n\n***************ADMIN OVERRIDE: SET ROUND TO NIGHT***************')
	client.set_game_time(gameid, 20)

	print ('\n\n***************ADMIN OVERRIDE: CREATE TREASURE HOARD AT UT TOWER***************')
	print ('***************ADMIN OVERRIDE: ADD \"SILVER PLATING\" TO TREASURE HOARD***************')
	client.set_treasure(gameid, 30.286190, -97.739346)
	client.set_treasure_item(gameid, 'Silver Plating', 7)

	print ('\n\n***************ADMIN OVERRIDE: MOVE TOBY TO NEAR TREASURE HOARD***************')
	client.set_location(gameid, 'toby', 30.286246, -97.739351)


	print ('\n\n***************UPDATE GAME (USER: TOBY)***************')
	client.update_game('toby', 'paper1', gameid, 30.286246, -97.739351)

	print ('\n\n***************ADMIN OVERRIDE: MOVE MICHAEL TO NEAR TOBY***************')
	client.set_location(gameid, 'michael', 30.286603, -97.739112)

	print ('\n\n***************UPDATE GAME (USER: MICHAEL)***************')
	client.update_game('michael', 'paper1', gameid, 30.286603, -97.739112)



	print ('\n***************MICHAEL ATTACKS TOBY***************')
	client.attack_villagers('michael', 'paper1', gameid, 'toby')	


	print ('\n\n***************MICHAEL TRIES TO ATTACK RYAN (who is not nearby)***************')
	client.attack_villagers('michael', 'paper1', gameid, 'ryan')	


	print ('\n***************ADMIN OVERRIDE: MOVE DWIGHT TO NEAR MICHAEL***************')
	client.set_location(gameid, 'dwight', 30.286246, -97.739351)

	print ('\n***************MICHAEL TRIES TO ATTACK DWIGHT (Michael is in a cooldown period)***************')
	client.attack_villagers('michael', 'paper1', gameid, 'dwight')	



	print ('\n***************VOTING***************')
	client.cast_vote('michael', 'paper1', gameid, 'toby')
	client.cast_vote('michael', 'paper1', gameid, 'dwight')
	client.cast_vote('pam', 'paper1', gameid, 'jim')
	client.cast_vote('jim', 'paper1', gameid, 'dwight')

	print ('\n>>Count votes:')
	client.count_votes('michael', 'paper1', gameid)


	print('\n>>Give Items')
	client.give_item(gameid, 'andy', 'Chain Mail')
	client.give_item(gameid, 'pam', 'Silver Dagger')


	print ('\n\n***************ADMIN OVERRIDE: SET ROUND TO Day***************')
	client.set_game_time(gameid, 7)

	print ('\n\n***************ADMIN OVERRIDE: SET ROUND TO Night***************')
	client.set_game_time(gameid, 20)


	print ('\n\n***************ADMIN OVERRIDE: SET EVERYONE (EXCEPT JIM) TO DEAD***************')
	client.set_dead(gameid, 'michael')
	client.set_dead(gameid, 'dwight')
	client.set_dead(gameid, 'pam')
	client.set_dead(gameid, 'ryan')
	client.set_dead(gameid, 'andy')
	client.set_dead(gameid, 'angela')
	client.set_dead(gameid, 'toby')


	print ('\n\n***************PlAYER MOVES TO TRIGGER GAME OVER***************')
	client.set_location(gameid, 'michael', 30.288688, -97.739757)
	client.update_game('michael', 'paper1', gameid, 30.288688, -97.739757)




main()