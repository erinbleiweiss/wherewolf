package edu.utexas.bleiweiss.wherewolf;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import edu.utexas.bleiweiss.wherewolf.model.SigninRequest;
import edu.utexas.bleiweiss.wherewolf.model.SigninResponse;


public class LoginActivity extends Activity {
	private static final String TAG = "loginactivity";
	private static final int GET_TEXT_REQUEST_CODE = 1;

	
    private class SigninTask extends AsyncTask<Void, Integer, SigninResponse> {

        @Override
        protected SigninResponse doInBackground(Void... request) {

            final EditText nameTV = (EditText) findViewById(R.id.usernameText);
            final EditText passTV = (EditText) findViewById(R.id.passwordText);
            
            String username = nameTV.getText().toString();
            String password = passTV.getText().toString();
            
            SigninRequest signinRequest = new SigninRequest(username, password);
            return signinRequest.execute(new WherewolfNetworking());
            
        }

        protected void onPostExecute(SigninResponse result) {

            Log.v(TAG, "Signed in user has player id " + result.getPlayerID());
                        
            if (result.getStatus().equals("success")) {
            	
                final EditText nameTV = (EditText) findViewById(R.id.usernameText);
                final EditText passTV = (EditText) findViewById(R.id.passwordText);
                
            	WherewolfPreferences pref = new WherewolfPreferences(LoginActivity.this);
            	pref.setCreds(nameTV.getText().toString(), passTV.getText().toString());
            	
//                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
//    			Editor editor = prefs.edit();
//    		    editor.putString("username", nameTV.getText().toString());
//    		    editor.putString("password", passTV.getText().toString());
//    		    editor.commit();
                 
                Log.v(TAG, "Signing in");
                Intent intent = new Intent(LoginActivity.this, GameSelectionActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right,
                        R.anim.slide_out_left);
            } else {
                // do something with bad password
    			Toast toast = new Toast(getApplicationContext());
    	        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
    	        toast.setDuration(Toast.LENGTH_LONG);
    	        
    	        toast.setView(getLayoutInflater().inflate(R.layout.login_error_toast,null));

    	        toast.show();
            }

        }

        

    }
	
	
	
	
	
	
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        		
        Log.i(TAG, "created the login activity");
        
//      SharedPreferences prefs = getSharedPreferences("edu.utexas.bleiweiss.wherewolf.prefs", Context.MODE_PRIVATE);
//		Editor editor = prefs.edit();
//		editor.clear();
//		editor.commit();
        
        
//		usernameEdit = (EditText) findViewById(R.id.usernameText);
//		passwordEdit = (EditText) findViewById(R.id.passwordText);
		
        WherewolfPreferences pref = new WherewolfPreferences(LoginActivity.this);
        String storedUsername = pref.getUsername();
        String storedPassword = pref.getPassword();
        
        if (!storedUsername.isEmpty() && !storedPassword.isEmpty())
        {
        	int currentGameId = pref.getCurrentGameID();
        	if (currentGameId != 0)
        	{
    			Intent explicitIntent = new Intent(LoginActivity.this, MainScreenActivity.class);
    			startActivityForResult(explicitIntent, GET_TEXT_REQUEST_CODE);
        	}
        	else
        	{
	        	Log.v(TAG, "Saved username and password detected. Launching game selection screen");
				Intent explicitIntent = new Intent(LoginActivity.this, GameSelectionActivity.class);
				startActivityForResult(explicitIntent, GET_TEXT_REQUEST_CODE);
        	}
        }
        
        EditText usernameEditText = (EditText) findViewById(R.id.usernameText);
        usernameEditText.setText(storedUsername);

        EditText passwordEditText = (EditText) findViewById(R.id.passwordText);
        passwordEditText.setText(storedPassword);
        
        final Button registerButton = (Button) findViewById(R.id.registerButton);
        registerButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Log.v(TAG, "Register button was pressed.");
				Intent explicitIntent = new Intent(LoginActivity.this, RegisterActivity.class);
				startActivityForResult(explicitIntent, GET_TEXT_REQUEST_CODE);
			}
		});
     
        final Button loginButton = (Button) findViewById(R.id.loginButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Log.v(TAG, "Login button was pressed.");
	            
				new SigninTask().execute();
			}
		});
        
    }



    
    
    
    
    

	@Override
	protected void onStart() {
		Log.i(TAG, "started the login activity");
		super.onStart();
	}

	@Override
	protected void onRestart() {
		Log.i(TAG, "restarted the login activity");
		super.onRestart();
	}

	@Override
	protected void onResume() {
		Log.i(TAG, "resumed the login activity");
		super.onResume();
	}

	@Override
	protected void onPause() {
		Log.i(TAG, "pause the login activity");
		super.onPause();
	}

	@Override
	protected void onStop() {
		Log.i(TAG, "stopped the login activity");
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		Log.i(TAG, "destroyed the login activity");
		super.onDestroy();
	}
    
    
    

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.login, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//        if (id == R.id.action_settings) {
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }
}
