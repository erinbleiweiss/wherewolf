package edu.utexas.bleiweiss.wherewolf;

import java.util.List;

import edu.utexas.bleiweiss.wherewolf.model.Player;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
 

public class PlayerAdapter extends ArrayAdapter<Player> {
 
      List<Player>   data;
      Context context;
      int layoutResID;
      String clickedPlayerName;

private boolean isNight;

public PlayerAdapter(Context context, int layoutResourceId, List<Player> data) {
	  super(context, layoutResourceId, data);
 
      this.data=data;
      this.context=context;
      this.layoutResID=layoutResourceId;
      isNight = false;
      
      // TODO Auto-generated constructor stub
}

public void setIsNight(boolean isNight){
	this.isNight = isNight;
}

public boolean checkIsNight(){
	return isNight;
}


@Override
public View getView(final int position, View convertView, ViewGroup parent) {
 
      NewsHolder holder = null;
         View row = convertView;
          holder = null;
 
        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResID, parent, false);
 
            holder = new NewsHolder();
 
            holder.profPic = (ImageView)row.findViewById(R.id.prof_pic);
            holder.playerName = (TextView)row.findViewById(R.id.player_name);
            holder.numVotes = (TextView)row.findViewById(R.id.num_votes);
            holder.attackVoteButton=(Button)row.findViewById(R.id.attack_vote_button);
            row.setTag(holder);
        }
        else
        {
            holder = (NewsHolder)row.getTag();
        }
 
        ///////// *** Added (ItemRow) cast
        Player playerdata = (Player) data.get(position);
        holder.profPic.setImageDrawable(playerdata.getProfPic());
        holder.playerName.setText(playerdata.getPlayerName());
        
        if (isNight)
        {
        	holder.attackVoteButton.setText("Attack");
        	holder.numVotes.setText("");
        }
        else
        {
        	holder.attackVoteButton.setText("Vote");
            holder.numVotes.setText("Votes: "+playerdata.getNumVotes());
        }

        holder.attackVoteButton.setOnClickListener(new View.OnClickListener() {
        	 
            @Override
            public void onClick(View v) {
	        	 Player clickedPlayer = (Player) MainScreenActivity.swipelistview.getItemAtPosition(position);
	        	 clickedPlayerName = clickedPlayer.getPlayerName();
            	
                  if (isNight)
                  {
                      Toast.makeText(context, "Attacked " +  clickedPlayerName,Toast.LENGTH_SHORT).show();

                  }
                  else
                  {
                      Toast.makeText(context, "Voted for " + clickedPlayerName,Toast.LENGTH_SHORT).show();
                  }
            }
        });
        
 
        return row;
 
}

static class NewsHolder{
 
	  ImageView profPic;
	  TextView playerName;
	  TextView numVotes;
      Button attackVoteButton;
      }
 
}