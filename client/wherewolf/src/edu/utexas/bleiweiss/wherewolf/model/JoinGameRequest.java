package edu.utexas.bleiweiss.wherewolf.model;

import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import edu.utexas.bleiweiss.wherewolf.WherewolfNetworking;
import edu.utexas.bleiweiss.wherewolf.exceptions.WherewolfNetworkingException;

public class JoinGameRequest extends BasicRequest{

	protected int gameId;
	
	public JoinGameRequest(String username, String password, int gameId){
		super (username, password);
		
		this.gameId = gameId;
	}

	public int getGameId() {
		return gameId;
	}

	public void setGameId(int gameId) {
		this.gameId = gameId;
	}

	@Override
	public String getURL() {
		String theURL = "/v1/game/" + Integer.toString(gameId) + "/lobby";
		return theURL;
	}

	@Override
	public RequestType getRequestType() {
		return RequestType.POST;
	}

	@Override
	public List<NameValuePair> getParameters() {
		return null;
	}



	@Override
	public JoinGameResponse execute(WherewolfNetworking net) {
		try{
			JSONObject response = net.sendRequest(this);
			if (response.getString("status").equals("success"))
			{
				return new JoinGameResponse("success", "joined game successfully");
			} else {
				String errorMessage = response.getString("error");
				return new JoinGameResponse("failure", errorMessage);
			}
		} catch (JSONException e){
			return new JoinGameResponse ("failure", "could not join game"); 
		} catch (WherewolfNetworkingException ex){
			return new JoinGameResponse ("faillure", "could not communicate with the server");
		}
	}
	
	
	
}
