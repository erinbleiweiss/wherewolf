package edu.utexas.bleiweiss.wherewolf.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import edu.utexas.bleiweiss.wherewolf.WherewolfNetworking;
import edu.utexas.bleiweiss.wherewolf.exceptions.WherewolfNetworkingException;

public class LeaveGameLobbyRequest extends BasicRequest {
	final String TAG = "LeaveGameRequest";
	
	private final int gameId;
	
	public LeaveGameLobbyRequest(String username, String password, int gameId){
		super (username, password);
		this.gameId = gameId;
	}

	public int getGameId() {
		return gameId;
	}
	
	@Override
	public String getURL() {
		return "/v1/game/" + Integer.toString(gameId) + "/lobby";
	}
	  
	@Override
	public RequestType getRequestType()
	{
		return RequestType.DELETE;
	}

	@Override
	public List<NameValuePair> getParameters() {
	List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
	urlParameters.add(new BasicNameValuePair("username", this.username));
	urlParameters.add(new BasicNameValuePair("password", this.password));
	urlParameters.add(new BasicNameValuePair("gameid", Integer.toString(gameId)));
	return urlParameters;
	}

	public LeaveGameLobbyResponse processResponse(JSONObject jObject) throws JSONException
	{
		JSONObject jResults = jObject.getJSONObject("results");
		int gameID = jResults.getInt("game_id");
		
		Log.v(TAG, "THIS WORKED" + Integer.toString(gameID));

		return new LeaveGameLobbyResponse("success",
				"Successfully left game");
	}

	@Override
	public LeaveGameLobbyResponse execute(WherewolfNetworking net) {
	      
		try {
			JSONObject jObject = net.sendRequest(this);
	          
			String status = jObject.getString("status");
				          
			if (status.equals("success"))
			{
				Log.v(TAG, jObject.getString("message"));
				return new LeaveGameLobbyResponse("success", "left game");
			} else {
				String errorMessage = jObject.getString("error");
				Log.v(TAG, errorMessage);
				return new LeaveGameLobbyResponse("failure", errorMessage);
			}
	          
		} 
		
		catch (WherewolfNetworkingException ex) {
			return new LeaveGameLobbyResponse("failure", "could not communicate with server.");
		}

		catch (JSONException e) {
			return new LeaveGameLobbyResponse("failure", "could not parse JSON.");
		}

	      
	}

}
