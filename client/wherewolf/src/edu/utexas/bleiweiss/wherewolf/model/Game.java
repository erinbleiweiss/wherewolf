package edu.utexas.bleiweiss.wherewolf.model;


public class Game {
	int gameId;
	String gameName;
	String description;
	private int status;
	private String adminName;
	private String adminUsername;
	
	public Game(int gameId, String gameName, String adminName) {
		super();
		this.gameId = gameId;
		this.gameName = gameName;
		this.adminName = adminName;
	}
	
	public Game(String gameName, String description, int gameId) {
		super();
		this.gameName = gameName;
		this.description = description;
	}
	
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getGameId() {
		return Integer.toString(gameId);
	}

	public void setGameId(int gameId) {
		this.gameId = gameId;
	}

	public String getGameName() {
		return this.gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	public String getAdminName() {
		return this.adminName;
	}

	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}
	
	public String getAdminUsername() {
		return adminUsername;
	}

	public void setAdminUsername(String adminUsername) {
		this.adminUsername = adminUsername;
	}
		
}
