package edu.utexas.bleiweiss.wherewolf.model;

public class JoinGameResponse extends BasicResponse {
	
	public JoinGameResponse(String status, String errorMessage){
		super(status, errorMessage);
	}

}
