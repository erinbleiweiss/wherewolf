package edu.utexas.bleiweiss.wherewolf.model;

import android.graphics.drawable.Drawable;

public class Player {
	private int playerId;
	private Drawable profPic;
	private String profPicUrl;
	private String playerName;
	private int numVotes;
	
	public Player(int playerId, Drawable profPic, String profPicUrl, String name, int numVotes) {
		super();
		this.playerId = playerId;
		this.profPic = profPic;
		this.profPicUrl = profPicUrl;
		this.playerName = name;
		this.numVotes = 0;
	}

	public String getPlayerId() {
		return Integer.toString(this.playerId);
	}

	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}

	public String getPlayerName() {
		return this.playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public String getProfPicUrl() {
		return this.profPicUrl;
	}

	public void setprofPicUrl(String profPicUrl) {
		this.profPicUrl = profPicUrl;
	}

	public Drawable getProfPic() {
		return this.profPic;
	}
	
	public void setProfPic(Drawable profPic) {
		this.profPic = profPic;
	}

	public String getNumVotes() {
		return Integer.toString(this.numVotes);
	}

	public void setNumVotes(int numVotes) {
		this.numVotes = numVotes;
	}


}
