package edu.utexas.bleiweiss.wherewolf.model;

public class RegisterResponse extends BasicResponse {

	  public RegisterResponse(String status, String errorMessage) {
	      super(status, errorMessage);
	  }
	  
	  public RegisterResponse(String status, String errorMessage, int playerID) {
	      super(status, errorMessage);
	      
	  }

}
