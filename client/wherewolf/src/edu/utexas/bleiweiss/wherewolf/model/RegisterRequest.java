package edu.utexas.bleiweiss.wherewolf.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import edu.utexas.bleiweiss.wherewolf.WherewolfNetworking;
import edu.utexas.bleiweiss.wherewolf.exceptions.WherewolfNetworkingException;

public class RegisterRequest extends BasicRequest {
	final String TAG = "RegisterRequest";
	
	private final String firstName;
	private final String lastName;
	private final String avatar;
	
	  public RegisterRequest (String username, String password, String firstName, String lastName, String avatar)
	  {
	      super(username, password);
	      this.firstName = firstName;
	      this.lastName = lastName;
	      this.avatar = avatar;
	  }
	  
	  /**
	  * Put the URL to your API endpoint here
	  */
	  @Override
	  public String getURL() {
	      return "/v1/register";
	  }
	  
	  public String getUsername(){
		  return this.username;
	  }
	  
	  public String getPassword(){
		  return this.password;
	  }
	  
	  public String getFirstName(){
		  return this.firstName;
	  }
	  
	  public String getLastName(){
		  return this.lastName;
	  }
	  
	  public String getAvatar(){
		  return this.avatar;
	  }
	  
	  
	  @Override
	  public List<NameValuePair> getParameters() {
		  List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		  urlParameters.add(new BasicNameValuePair("username", this.username));
		  urlParameters.add(new BasicNameValuePair("password", this.password));
		  urlParameters.add(new BasicNameValuePair("firstname", this.firstName));
		  urlParameters.add(new BasicNameValuePair("lastname", this.lastName));
		  urlParameters.add(new BasicNameValuePair("avatar", this.avatar));
		  return urlParameters;
	  }

	  @Override
	  public RequestType getRequestType() {
	      return RequestType.POST;
	  }

	  	  
	  @Override
	  public RegisterResponse execute(WherewolfNetworking net) {
	  
	      try {
	          JSONObject response = net.sendRequest(this);
	          
	          if (response.getString("status").equals("success"))
	          {
	              return new RegisterResponse("success", "registered successfully");
	          } else {
	              
	              String errorMessage = response.getString("error");
	              return new RegisterResponse("failure", errorMessage);
	          }
	      } catch (JSONException e) {
	    	  Log.v(TAG, "error block 1");
	    	  	
	          return new RegisterResponse("failure", "register not working");
	      } catch (WherewolfNetworkingException ex)
	      {
	    	  Log.v(TAG, "error block 2");
	          return new RegisterResponse("failure", "could not communicate with the server");
	      }
	      
	      
	      
	  }
}
