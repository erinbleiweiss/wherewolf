package edu.utexas.bleiweiss.wherewolf.model;

public class LeaveGameResponse extends BasicResponse{
	
	protected Game game = null;

	public LeaveGameResponse(String status, String message){
		super (status, message);
		
	}

	public LeaveGameResponse(String username, String password, Game game){
		super (username, password);
		
		this.game = game;
	}

	public LeaveGameResponse(String status, String message, int gameId){
		super (status, message);
		
		gameId = game.gameId;
	}

	public Game getGame() {
		return game;
	}
	
	public int getGameId(){
		return game.gameId;
	}
	
	
}
