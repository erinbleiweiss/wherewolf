package edu.utexas.bleiweiss.wherewolf;

import java.util.ArrayList;

import edu.utexas.bleiweiss.wherewolf.model.Player;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.graphics.drawable.Drawable;


public class PlayerAdapter_Lobby extends ArrayAdapter<Player>{
	public PlayerAdapter_Lobby(Context context, ArrayList<Player> players) {
        super(context, 0, players);
     }
	
	 @Override
	 public View getView(int position, View convertView, ViewGroup parent) {
	    // Get the data item for this position
	    Player player = getItem(position);    
	    // Check if an existing view is being reused, otherwise inflate the view
	    if (convertView == null) {
	       convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_player, parent, false);
	    }
	    // Lookup view for data population
	    ImageView profileImg = (ImageView) convertView.findViewById(R.id.prof_pic);
	    TextView playerNameTV = (TextView) convertView.findViewById(R.id.player_name);

	    // Populate the data into the template view using the data object
	    if (player.getProfPicUrl().equals("villagerfemale")){
	    	profileImg.setImageResource(R.drawable.villagerfemale);
        }
        else
        {
        	profileImg.setImageResource(R.drawable.villagermale);
        }
	    playerNameTV.setText(player.getPlayerName());
	    
	    return convertView;
	}
}