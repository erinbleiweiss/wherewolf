package edu.utexas.bleiweiss.wherewolf.exceptions;

 public final class WherewolfNetworkingException extends Exception
 {

	 private static final long serialVersionUID = 1L;
	 
	 public WherewolfNetworkingException(String msg)
	 {
		 super(msg);
	 }
}