package edu.utexas.bleiweiss.wherewolf;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import edu.utexas.bleiweiss.wherewolf.model.CreateGameRequest;
import edu.utexas.bleiweiss.wherewolf.model.CreateGameResponse;
import edu.utexas.bleiweiss.wherewolf.model.Game;

public class CreateGameActivity extends Activity {
	private static final String TAG = "CreateGameActivity";
	private EditText gameNameEdit;
	private EditText gameDescriptionEdit;
	
	
	public class CreateGameTask extends AsyncTask<Void, Integer, CreateGameResponse>{
		@Override
		protected CreateGameResponse doInBackground(Void... request){
			
			final EditText gameNameTV = (EditText) findViewById(R.id.game_name_text);
			final EditText gameDescriptionTV = (EditText) findViewById(R.id.gameDescription);
			
			String gameName = gameNameTV.getText().toString();
			String description = gameDescriptionTV.getText().toString();
			
			SharedPreferences sharedPreferences = getSharedPreferences("edu.utexas.bleiweiss.wherewolf.prefs", Context.MODE_PRIVATE);
			String username = sharedPreferences.getString("username", "");
			String password = sharedPreferences.getString("password", "");
			
	        Game game = new Game(gameName, description, 0);
	        
			CreateGameRequest createGameRequest = new CreateGameRequest(username, password, game);
			return createGameRequest.execute(new WherewolfNetworking());
		}
		
		protected void onPostExecute(CreateGameResponse result){
			
			if (result.getStatus().equals("success"))
			{

				SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext()); 
				// SharedPreferences sharedPreferences = getSharedPreferences("edu.utexas.bleiweiss.wherewolf.prefs", Context.MODE_PRIVATE);
				Editor editor = sharedPreferences.edit();
				editor.putInt("gameId", Integer.parseInt(result.getGame().getGameId()));
				editor.commit();
				
				CreateGameActivity.this.finish();
				overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
				
			} else{
			    Toast.makeText(CreateGameActivity.this, result.getErrorMessage(), Toast.LENGTH_LONG).show();
			}
			
		}
		
		
	}
		
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_game);
		
		final Button createGameButton = (Button) findViewById(R.id.create_game_button);	
		
		createGameButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				new CreateGameTask().execute();
				
				// Log.v(TAG, "User created a new game");
			}
		});
	}
	
	private void createGameClicked() {
		Log.i(TAG,"Entered createGameClicked()");		
		String gameName= gameNameEdit.getText().toString();
		if (gameName.length() != 0){
			Log.i(TAG,"Creating Game Succeeded");
			this.finish();	
		}
		
		else{
			Toast toast = new Toast(getApplicationContext());

	        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
	        toast.setDuration(Toast.LENGTH_LONG);
	        
	        toast.setView(getLayoutInflater().inflate(R.layout.create_game_toast,null));

	        toast.show();
		}
	}


	

	
	
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.create_game, menu);
//		return true;
//	}
//
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//		// Handle action bar item clicks here. The action bar will
//		// automatically handle clicks on the Home/Up button, so long
//		// as you specify a parent activity in AndroidManifest.xml.
//		int id = item.getItemId();
//		if (id == R.id.action_settings) {
//			return true;
//		}
//		return super.onOptionsItemSelected(item);
//	}
}
