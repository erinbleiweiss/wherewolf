package edu.utexas.bleiweiss.wherewolf;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import edu.utexas.bleiweiss.wherewolf.model.Game;
import edu.utexas.bleiweiss.wherewolf.model.GetGamesRequest;
import edu.utexas.bleiweiss.wherewolf.model.GetGamesResponse;
import edu.utexas.bleiweiss.wherewolf.model.GetPlayersRequest;
import edu.utexas.bleiweiss.wherewolf.model.GetPlayersResponse;
import edu.utexas.bleiweiss.wherewolf.model.LeaveGameLobbyRequest;
import edu.utexas.bleiweiss.wherewolf.model.LeaveGameLobbyResponse;
import edu.utexas.bleiweiss.wherewolf.model.LeaveGameRequest;
import edu.utexas.bleiweiss.wherewolf.model.LeaveGameResponse;
import edu.utexas.bleiweiss.wherewolf.model.Player;

public class GameLobbyActivity extends ListActivity {
private static final String TAG = "gamelobbyactivity";

int gameId;

PlayerAdapter_Lobby adapter;
private static ArrayList<Player> arrayOfPlayers = new ArrayList<Player>();
	 
	private class GetPlayersTask extends AsyncTask<Void, Integer, GetPlayersResponse>{
	
	@Override
	protected GetPlayersResponse doInBackground(Void... request){
		
		WherewolfPreferences pref = new WherewolfPreferences(GameLobbyActivity.this);
		String username = pref.getUsername();
		String password = pref.getPassword();
		
		GetPlayersRequest getPlayersRequest = new GetPlayersRequest(username, password, gameId);
		return getPlayersRequest.execute(new WherewolfNetworking());
	}
	
	protected void onPostExecute(GetPlayersResponse result){
		
        if (result.getStatus().equals("success")) {
        	
        	arrayOfPlayers.clear();
        	
			JSONArray jArray = result.getPlayers();
			
			for (int i=0; i<jArray.length(); i++){
				try{
					JSONObject oneObject = jArray.getJSONObject(i);
					// Pulling Items from array
					int playerId = oneObject.getInt("playerid");
					Drawable profPic = null;
					String profPicUrl = oneObject.getString("avatar");
					String playerName = oneObject.getString("playername");
					int numVotes = 0;
										
					arrayOfPlayers.add(new Player(playerId, profPic, profPicUrl, playerName, numVotes)); 	
					adapter.notifyDataSetChanged();
				}                                    
				catch(JSONException e){
					Log.v(TAG, "JSON Exception was thrown");
				}
			}
			

        } else {
            // do something with bad password
        	Toast.makeText(GameLobbyActivity.this, result.getErrorMessage(), Toast.LENGTH_LONG).show();
        }

		
	}
}


	private class LeaveGameLobbyTask extends AsyncTask<Void, Integer, LeaveGameLobbyResponse>{
		
		@Override
		protected LeaveGameLobbyResponse doInBackground(Void... request){
			
			SharedPreferences sharedPreferences = getSharedPreferences("edu.utexas.bleiweiss.wherewolf.prefs", Context.MODE_PRIVATE);
			String username = sharedPreferences.getString("username", "");
			String password = sharedPreferences.getString("password", "");
			
			LeaveGameLobbyRequest leaveGameLobbyRequest = new LeaveGameLobbyRequest(username, password, gameId);
			return leaveGameLobbyRequest.execute(new WherewolfNetworking());
		}
		
		protected void onPostExecute(LeaveGameLobbyResponse result){
			
            if (result.getStatus().equals("success")) {
            	WherewolfPreferences pref = new WherewolfPreferences(GameLobbyActivity.this);
            	pref.setCurrentGameID(0);
            } else {
                // do something with bad password
            	Toast.makeText(GameLobbyActivity.this, result.getErrorMessage(), Toast.LENGTH_LONG).show();
            }

			
		}
	}
	
	


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game_lobby);
		
    	WherewolfPreferences pref = new WherewolfPreferences(GameLobbyActivity.this);
		gameId = pref.getCurrentGameID();
		
		new GetPlayersTask().execute();
		adapter = new PlayerAdapter_Lobby(this, arrayOfPlayers);
		final ListView playerListView = getListView();
		
		playerListView.setAdapter(adapter);
		
		playerListView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Log.v(TAG, "Tap player, doing nothing");
				// do nothing
			}
		});
		
		final Button startGameButton = (Button) findViewById(R.id.start_game_button);		
		startGameButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Log.v(TAG, "User pressed the create new game button");
				Intent createGameIntent = new Intent(GameLobbyActivity.this, MainScreenActivity.class);
				startActivity(createGameIntent);			
			}
		});	
		 		
	}
	
	
	
	
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK ) {
	    	
	    	new LeaveGameLobbyTask().execute();
	    	
	    }
	    return super.onKeyDown(keyCode, event);
	}	
	
	
	
	
	

	@Override
	protected void onStart() {
		Log.i(TAG, "started the game lobby activity");
		super.onStart();
		
		adapter.clear();
		new GetPlayersTask().execute();
	}

	@Override
	protected void onRestart() {
		Log.i(TAG, "restarted the game lobby activity");
		super.onRestart();
		
		adapter.clear();
		new GetPlayersTask().execute();
	}

	@Override
	protected void onResume() {
		Log.i(TAG, "resumed the game lobby activity");
		super.onResume();
		
		adapter.clear();
		new GetPlayersTask().execute();
		
	}

	@Override
	protected void onPause() {
		Log.i(TAG, "pause the game lobby activity");
		super.onPause();
	}

	@Override
	protected void onStop() {
		Log.i(TAG, "stopped the game lobby activity");
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		Log.i(TAG, "destroyed the game lobby activity");
		super.onDestroy();
	}	
	
	
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.game_selection, menu);
//		return true;
//	}
//
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//		// Handle action bar item clicks here. The action bar will
//		// automatically handle clicks on the Home/Up button, so long
//		// as you specify a parent activity in AndroidManifest.xml.
//		int id = item.getItemId();
//		if (id == R.id.action_settings) {
//			return true;
//		}
//		return super.onOptionsItemSelected(item);
//	}
}