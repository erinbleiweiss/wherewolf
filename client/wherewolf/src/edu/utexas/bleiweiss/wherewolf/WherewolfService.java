package edu.utexas.bleiweiss.wherewolf;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.Process;
import android.util.Log;
import android.widget.Toast;
import edu.utexas.bleiweiss.wherewolf.model.ChangedLocationRequest;
import edu.utexas.bleiweiss.wherewolf.model.ChangedLocationResponse;
import edu.utexas.bleiweiss.wherewolf.model.Game;
import edu.utexas.bleiweiss.wherewolf.model.Player;

public class WherewolfService extends Service implements LocationListener{
	
	private static final String TAG = "WherewolfService";
	
	String urlAddress = "http://www.wherewolf.com";
	
	// number of milliseconds before a location update
	private static final int REPORT_PERIOD = 5000;
	private static final int MIN_CHANGE = 0;
	
	// allows us to prevent the CPU from going to sleep.
	private WakeLock wakeLock;
	
	 // allows us to register updates to the GPS system
	private LocationManager locationManager;
	
	
    private boolean isNight = false;
    private Game currentGame = null;
    private Player currentPlayer = null;

    private Looper mServiceLooper;
	private Handler handler;
    
	@Override
	public void onCreate(){
		super.onCreate();
		
		Log.v(TAG, "Service is working");
		// Toast.makeText(this, "Service is working", Toast.LENGTH_SHORT).show();
		
	    HandlerThread thread = new HandlerThread("WherewolfThread",
	            Process.THREAD_PRIORITY_BACKGROUND);

	    thread.start();

	    mServiceLooper = thread.getLooper();
	    handler = new Handler(mServiceLooper);
	        
		locationManager = (LocationManager) getApplicationContext()
				.getSystemService(Context.LOCATION_SERVICE);
		
//		locationManager.requestLocationUpdates(REPORT_PERIOD, MIN_CHANGE,
//				new Criteria(), this, mServiceLooper);

		PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
		
		wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "DoNotSleep");

		// wakeLock.acquire();  CPU can't sleep
		// wakeLock.release();  CPU allowed to sleep
		
		// Toast.makeText(this, "Calling set night", Toast.LENGTH_SHORT).show();
		setNight();
	}
    
    
	public void setNight()
	{
		
		handler.post(new Runnable(){
			@Override
			public void run ()
			{
				// Toast.makeText(WherewolfService.this, "Set night", Toast.LENGTH_SHORT).show();
				wakeLock.acquire();
				
				// makes location updates happen every 5 seconds, with no minimum 
				// distance for change notification     
				locationManager.requestLocationUpdates(
						LocationManager.GPS_PROVIDER, REPORT_PERIOD, MIN_CHANGE,
						WherewolfService.this);
				
				isNight = true;
			}
		});

		
	}
	
	public void setDay()
	{
		handler.post(new Runnable() {
			@Override
			public void run() {
				// Log.i(TAG, "Setting to day, turning off tracking");

				if (isNight) {

					if (wakeLock.isHeld()) {
						wakeLock.release();
					}

					locationManager.removeUpdates(WherewolfService.this);

					isNight = false;
					Log.i(TAG, "Setting to day, turning off tracking");
	                  
				}
			}
		});
	}
	
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startID)
	{
		return START_STICKY;
	}
	
	
	
	private void showLocation(String msg){
		Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
	}
	
	@Override
	public void onLocationChanged(Location location){
		Log.v(TAG, "onLocationChanged");
		
		
		if (location != null){

			WherewolfPreferences pref = new WherewolfPreferences(WherewolfService.this);

			if ((pref.getLatitude() == "") || (pref.getLongitude() == ""))
			{
            	pref.setLatitude(Double.toString(location.getLatitude()));
            	pref.setLongitude(Double.toString(location.getLongitude()));
            	
				final String firstLocMsg = "location is " 
						+ location.getLatitude() + " "
						+ location.getLongitude();
				
				showLocation(firstLocMsg);
			}
						
			double tolerance = 0.0001;
			
			// Log.v(TAG, Double.toString((Math.abs(location.getLongitude() - (double)pref.getLongitude()))));
			
			try {
				if ((Math.abs(location.getLatitude() - Double.parseDouble(pref.getLatitude())) > tolerance) || 
						((Math.abs(location.getLongitude() - Double.parseDouble(pref.getLongitude())) > tolerance)))
				{
					final String locMsg = "location changed to " 
							+ location.getLatitude() + " "
							+ location.getLongitude();
					
					
	            	pref.setLatitude(Double.toString(location.getLatitude()));
	            	pref.setLongitude(Double.toString(location.getLongitude()));
	            	
					showLocation(locMsg);
					Log.i(TAG, locMsg);
				}
			} finally {
            	pref.setLatitude(Double.toString(location.getLatitude()));
            	pref.setLongitude(Double.toString(location.getLongitude()));
			}
			
			ChangedLocationRequest request = new ChangedLocationRequest(pref.getUsername(),
					pref.getPassword(), pref.getCurrentGameID(), location.getLatitude(), location.getLongitude());
			ChangedLocationResponse result = request.execute(new WherewolfNetworking());
			
			
			if (result.getStatus().equals("success"))
			{
				pref.setTime(result.getCurrentTime()); 
				// Log.v(TAG, Long.toString(result.getCurrentTime()));
			}
			
			
		}
		else{
			Log.v(TAG, "location is null");
			Toast.makeText(this, "Location is null", Toast.LENGTH_SHORT).show();
		}
	}
	
	
	
	
	private LocationListener listener = new LocationListener(){
		@Override
		public void onLocationChanged(Location location)
		{
			if (location != null)
			{
				// do something with new location
				double latitude = location.getLatitude();
				double longitude = location.getLongitude();
				
				// TODO: send the notification to the server here.
				// Create a UpdateGameRequest class
				// request.execute(new WherewolfNetworking())
				Log.v("GPS", "Lat is " + latitude + " longitude " + longitude);
			}
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub
			
		}
	};
	
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void onDestroy(){
		locationManager.removeUpdates(listener);
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}

}
