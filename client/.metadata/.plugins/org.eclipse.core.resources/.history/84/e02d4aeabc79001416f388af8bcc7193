package edu.utexas.bleiweiss.wherewolf;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import edu.utexas.bleiweiss.wherewolf.exceptions.WherewolfNetworkingException;
import edu.utexas.bleiweiss.wherewolf.model.BasicRequest;
import edu.utexas.bleiweiss.wherewolf.model.BasicRequest.RequestType;
import edu.utexas.bleiweiss.wherewolf.model.CreateGameRequest;
import edu.utexas.bleiweiss.wherewolf.model.CreateGameResponse;
import edu.utexas.bleiweiss.wherewolf.model.Game;
import edu.utexas.bleiweiss.wherewolf.model.JoinGameRequest;
import edu.utexas.bleiweiss.wherewolf.model.JoinGameResponse;
import edu.utexas.bleiweiss.wherewolf.model.SigninRequest;
import edu.utexas.bleiweiss.wherewolf.model.SigninResponse;

import android.util.Base64;
import android.util.Log;

public class WherewolfNetworking {

	// when you submit, replace this IP address with EC2 Instance (load balancer?)
	//private static final String fullhost = "http://54.191.43.168:5000/";
	private static final String fullhost = "http://192.168.1.77:5000";
	
	// IP for built-in emulator that's not Genymotion
	// private static final String fullhost = "http://10.0.2.2:5000/v1";

	private static final String TAG = "WhereWolf";
	
	
	public WherewolfNetworking(){
		
	}
	
	public JSONObject sendRequest(BasicRequest basicRequest)
	          throws WherewolfNetworkingException {
	      
	      InputStream inputStream = null;
	      String result = null;

	      String url = fullhost + basicRequest.getURL();
	      RequestType requestType = basicRequest.getRequestType();
	      List<NameValuePair> payload = basicRequest.getParameters();
	      String username = basicRequest.getUsername();
	      String password = basicRequest.getPassword();

	      try {

	    	  
	    	  
	          final DefaultHttpClient httpClient = new DefaultHttpClient();

	          // HttpUriRequest request;
	          HttpResponse response;

	          HttpUriRequest request;

	          if (basicRequest.getRequestType() == RequestType.GET) {
	              request = new HttpGet(url);
	              
	              request.setHeader("Content-type", "application/json");
	              // add authentication stuff here.

	          } else if (requestType == RequestType.POST) {

	              HttpPost postRequest = new HttpPost(url);
	              postRequest.setHeader("Content-type",
	                      "application/x-www-form-urlencoded");

	              if (payload!=null) {

	                  postRequest.setEntity(new UrlEncodedFormEntity(payload));
	              }
	              
	              request = postRequest;
	              

	          } else if (requestType == RequestType.DELETE) {
	              
	              HttpDelete deleteRequest = new HttpDelete(url);
	          
	              request = deleteRequest;
	              request.setHeader("Content-type", "application/json");
	              
	          } else if (requestType == RequestType.PUT) {
	              
	              request = new HttpPut(url);
	              request.setHeader("Content-type", "application/json");
	              
	                  
	          } else {
	              throw new WherewolfNetworkingException(
	                      "Does not support the HTTP request type");
	          }

	          
	          
	          

	          if (!username.equals("")) {
	              String authorizationString = "Basic "
	                      + Base64.encodeToString(
	                              (username + ":" + password).getBytes(),
	                              Base64.NO_WRAP);
	              request.setHeader("Authorization", authorizationString);
	          }
	          
	          response = httpClient.execute(request);

	          HttpEntity entity = response.getEntity();

	          inputStream = entity.getContent();

	          BufferedReader reader = new BufferedReader(new InputStreamReader(
	                  inputStream, "UTF-8"), 8);
	          StringBuilder sb = new StringBuilder();

	          String line = null;
	          while ((line = reader.readLine()) != null) {
	              sb.append(line + "\n");
	          }

	          result = sb.toString();

	          try {

	              JSONObject json = new JSONObject(result);
	              return json;

	          } catch (JSONException ex) {
	              throw new WherewolfNetworkingException("Could not parse JSON");
	          }

	      } catch (Exception e) {
	          Log.e(TAG, "Problem with response from server" + e.toString());

	      } finally {
	          try {
	              if (inputStream != null)
	                  inputStream.close();
	          } catch (Exception ex) {
	              throw new WherewolfNetworkingException("Network problem");
	          }
	      }

	      throw new WherewolfNetworkingException("Network problem");

	  }
	  
	
	public boolean checkPassword(String username, String password){
		
		// getJsonFromServer is a custom function
		// requests
		// url, request type, username, password, payload
		String result = getJsonFromServer(fullhost + "checkpassword",
				RequestType.GET, username,
				password, null);
		
		if (result == null)
		{
			Log.e(TAG, "Got a strange response from server");
			return false;
		}
		
		try {
			Log.i(TAG, "Checking password returned " + result);
			JSONObject jObject = new JSONObject(result);
			
			String status = jObject.getString("status");
			
			if (status.equals("success"))
			{
				return true;
			}
		} catch (JSONException ex)
		{
			return false;
		}
		
		return false;
	}
	
	
	// ************************************ //
	public String getJsonFromServer(String url, RequestType requestType,
            String username, String password, List<NameValuePair> payload) {

        InputStream inputStream = null;
        String result = null;

        try {

            final DefaultHttpClient httpClient = new DefaultHttpClient();

            // HttpUriRequest request;
            HttpResponse response;

            if (requestType == RequestType.GET) {
                HttpGet getRequest = new HttpGet(url);
                getRequest.setHeader("Content-type", "application/json");

                // add authentication stuff here.
                if (!username.equals("")) {
                    String authorizationString = "Basic "
                            + Base64.encodeToString(
                                    (username + ":" + password).getBytes(),
                                    Base64.NO_WRAP);
                    getRequest.setHeader("Authorization", authorizationString);
                }

                response = httpClient.execute(getRequest);

            } else if (requestType == RequestType.POST) {

                HttpPost postRequest = new HttpPost(url);
                postRequest.setHeader("Content-type",
                        "application/x-www-form-urlencoded");

                if (!payload.equals("")) {

                    postRequest.setEntity(new UrlEncodedFormEntity(payload));
                }

                // add authentication stuff here.
                if (!username.equals("")) {
                    String authorizationString = "Basic "
                            + Base64.encodeToString(
                                    (username + ":" + password).getBytes(),
                                    Base64.NO_WRAP);
                    postRequest.setHeader("Authorization", authorizationString);
                }

                response = httpClient.execute(postRequest);

            } else {
                return null;
            }

            // execute the request here.
            // HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();

            inputStream = entity.getContent();

            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    inputStream, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }

            result = sb.toString();
        } catch (Exception e) {
            Log.e(TAG, "Problem with response from server" + e.toString());

        } finally {
            try {
                if (inputStream != null)
                    inputStream.close();
            } catch (Exception ex) {
            }
        }

        return result;
    }	
	// ************************************ //

	
	public JoinGameResponse joinGame(JoinGameRequest request) throws WherewolfNetworkingException
	{
		try{
			List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
			urlParameters.add(new BasicNameValuePair("game_name", Integer.toString(request.getGameId())));
			
			String result = getJsonFromServer(fullhost + "game",
					RequestType.POST, request.getUsername(),
					request.getPassword(), urlParameters);
			
			JSONObject jObject = new JSONObject(result);
			
			String status = jObject.getString("status");
			
			if (status.equals("success"))
			{
				return new JoinGameResponse("success", "Successfully joined the game");
			} else
			{
				String errorMessage = jObject.getString("errorMessage");
				return new JoinGameResponse("failure", errorMessage);
			}
			
			
		} catch (JSONException ex)
		{
			return new JoinGameResponse("failure", "Could not parse response from server");
		}
		
	}
	
	
	
	
	public ArrayList<Game> getGames(String username, String password) throws WherewolfNetworkingException{
		
		ArrayList<Game> games = new ArrayList<Game>();
		
		// /v1/games
		String result = getJsonFromServer(fullhost + "games", RequestType.GET, username, password, null);
		
		
		try{
			JSONObject jObject = new JSONObject(result);
			JSONArray jArray = jObject.getJSONArray("results");
			for (int i=0; i<jArray.length(); i++){
				try{
					JSONObject oneObject = jArray.getJSONObject(i);
					// Pulling Items from array
					int gameID = oneObject.getInt("game_id");
					String gameName = oneObject.getString("name");
					int adminName = oneObject.getString("admin_name");
					
					games.add(new Game(gameID, gameName, adminName));
					
					Log.i(TAG, "Game is" + gameName);
				}                                    
				catch(JSONException e){
					// oops
				}
			}
			
			
		} catch (JSONException ex){
			Log.e(TAG, "Problem parsing");
			throw new WherewolfNetworkingException("problem parsing result");
		}
		
		return games;

		
	}
	
	

	public CreateGameResponse createGame(CreateGameRequest request){
		try{
			List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
			urlParameters.add(new BasicNameValuePair("game_name", request.getGame().getGameName()));
			urlParameters.add(new BasicNameValuePair("description", request.getGame().getDescription()));
			
			String result = getJsonFromServer(fullhost + "game",
					RequestType.POST, request.getUsername(),
					request.getPassword(), urlParameters);
			
			Log.i(TAG, "Result was " + result);
			
			if (result == null){
				return new CreateGameResponse("failure", "Got no message from server");
			}
			
			JSONObject jObject = new JSONObject(result);
			
			String status = jObject.getString("status");
			Log.i(TAG, "Status for creating a game was " + status);

			if (status.equals("success")) {
				JSONObject jResult = jObject.getJSONObject("results");
				int gameId = jResult.getInt("game_id");
				
				return new CreateGameResponse("success", "Successfully created the game", 
						new Game(gameId, request.getGame().getGameName(), Integer.parseInt(request.getGame().getAdminId())));
			}
			
			
		}
		
		catch (JSONException ex){
			Log.e(TAG, "Problem parsing");
			return new CreateGameResponse("failure", "Could not parse the response");
		}
		
		return new CreateGameResponse("failure", "Some strange error occurred");
	}

	public ArrayList<Game> joinGame(String username, String password, int gameId) {
		// TODO Auto-generated method stub
		return null;
	}

	
	
	
	
	
	
	
	
}
