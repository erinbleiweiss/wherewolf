
package edu.utexas.bleiweiss.wherewolf;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import edu.utexas.bleiweiss.wherewolf.exceptions.WherewolfNetworkingException;
import edu.utexas.bleiweiss.wherewolf.model.Game;
import edu.utexas.bleiweiss.wherewolf.model.GetGamesRequest;
import edu.utexas.bleiweiss.wherewolf.model.GetGamesResponse;
import edu.utexas.bleiweiss.wherewolf.model.JoinGameRequest;
import edu.utexas.bleiweiss.wherewolf.model.JoinGameResponse;
import edu.utexas.bleiweiss.wherewolf.model.LeaveGameRequest;
import edu.utexas.bleiweiss.wherewolf.model.LeaveGameResponse;


public class GameSelectionActivity extends ListActivity {
	private static final String TAG = "gameselectionactivity";
	private ArrayList<Game> arrayOfGames = new ArrayList<Game>();
	
	GameAdapter adapter;
	Game clickedGame;
	
	private class GetGameListTask extends AsyncTask<Void, Integer, GetGamesResponse>{
		
		@Override
		protected GetGamesResponse doInBackground(Void... request){
			
			SharedPreferences sharedPreferences = getSharedPreferences("edu.utexas.bleiweiss.wherewolf.prefs", Context.MODE_PRIVATE);
			
			String username = sharedPreferences.getString("username", "");
			String password = sharedPreferences.getString("password", "");
						
			GetGamesRequest getGamesRequest = new GetGamesRequest(username, password);
			return getGamesRequest.execute(new WherewolfNetworking());
		}
		
		protected void onPostExecute(GetGamesResponse result){
			
            if (result.getStatus().equals("success")) {
            	
            	arrayOfGames.clear();
            	
    			JSONArray jArray = result.getGames();
    			
    			for (int i=0; i<jArray.length(); i++){
    				try{
    					JSONObject oneObject = jArray.getJSONObject(i);
    					// Pulling Items from array
    					int gameID = oneObject.getInt("game_id");
    					String gameName = oneObject.getString("name");
    					String adminName = oneObject.getString("admin_name");
    					
    					arrayOfGames.add(new Game(gameID, gameName, adminName)); 	
    					
    					adapter.notifyDataSetChanged();
    					
    					//Log.i(TAG, "Game is" + gameName);
    				}                                    
    				catch(JSONException e){
    					Log.v(TAG, "JSON Exception was thrown");
    				}
    			}
    			

            } else {
                // do something with bad password
            	Toast.makeText(GameSelectionActivity.this, result.getErrorMessage(), Toast.LENGTH_LONG).show();
            }

			
		}
	}
	
	
	
	private class LeaveGameTask extends AsyncTask<Void, Integer, LeaveGameResponse>{
		
		@Override
		protected LeaveGameResponse doInBackground(Void... request){
			
			SharedPreferences sharedPreferences = getSharedPreferences("edu.utexas.bleiweiss.wherewolf.prefs", Context.MODE_PRIVATE);
			String username = sharedPreferences.getString("username", "");
			String password = sharedPreferences.getString("password", "");
			
			LeaveGameRequest leaveGameRequest = new LeaveGameRequest(username, password, clickedGame);
			return leaveGameRequest.execute(new WherewolfNetworking());
		}
		
		protected void onPostExecute(LeaveGameResponse result){
			
            if (result.getStatus().equals("success")) {
            	Toast.makeText(GameSelectionActivity.this, "Deleted.", Toast.LENGTH_LONG).show();
            } else {
                // do something with bad password
            	Toast.makeText(GameSelectionActivity.this, result.getErrorMessage(), Toast.LENGTH_LONG).show();
            }

			
		}
	}
	
	
	
	
	
	private class JoinGameTask extends AsyncTask<Void, Integer, JoinGameResponse>{
		
		@Override
		protected JoinGameResponse doInBackground(Void... request){
			
			SharedPreferences sharedPreferences = getSharedPreferences("edu.utexas.bleiweiss.wherewolf.prefs", Context.MODE_PRIVATE);
			String username = sharedPreferences.getString("username", "");
			String password = sharedPreferences.getString("password", "");
			
			JoinGameRequest joinGameRequest = new JoinGameRequest(username, password, Integer.parseInt(clickedGame.getGameId()));
			return joinGameRequest.execute(new WherewolfNetworking());
		}
		
		protected void onPostExecute(JoinGameResponse result){
			
            if (result.getStatus().equals("success")) {
            	Toast.makeText(GameSelectionActivity.this, "Success", Toast.LENGTH_LONG).show();
            } else {
                // do something with bad password
            	Toast.makeText(GameSelectionActivity.this, result.getErrorMessage(), Toast.LENGTH_LONG).show();
            }

			
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        Log.i(TAG, "created the game selection activity");

		setContentView(R.layout.activity_game_selection);				

		new GetGameListTask().execute();
		adapter = new GameAdapter(this, arrayOfGames);
		final ListView gameListView = getListView();

		gameListView.setAdapter(adapter);
		
		//adapter.addAll(arrayOfGames);
		// adapter.notifyDataSetChanged();
		
		/////// Header
        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.item_game_header, gameListView,
                false);
                
        gameListView.addHeaderView(header, null, false);
		
        //////// End Header
        		
		
		gameListView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Log.v(TAG, "Selected game to join");
				
				clickedGame = (Game) gameListView.getItemAtPosition(position);
				int clickedGameId = Integer.parseInt(clickedGame.getGameId());
				
				
				
				Intent joinGameIntent = new Intent(GameSelectionActivity.this, GameLobbyActivity.class);
				joinGameIntent.putExtra("gameId", (int)clickedGameId);
				startActivity(joinGameIntent);				
				}
		});
		
		gameListView.setOnItemLongClickListener(new OnItemLongClickListener() {

            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                    final int idx, long id) {

            	
	    	    	AlertDialog.Builder deleteBuilder = new AlertDialog.Builder(GameSelectionActivity.this);
	    	    	deleteBuilder.setTitle("Delete");
	    	    	deleteBuilder.setMessage("Delete Game?");
	    	
	    	    	deleteBuilder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
	    				
	    				@Override
	    				public void onClick(DialogInterface dialog, int which) {

	    					clickedGame = (Game) gameListView.getItemAtPosition(idx);
	    	            	Log.v(TAG, "CLICKED GAME: " + clickedGame.getGameId());

	    					new LeaveGameTask().execute();
	    					new GetGameListTask().execute();
	    					dialog.dismiss();
	    					return;
	    				}
	    			});
	    	    	
	    	    	deleteBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
	    				
	    				@Override
	    				public void onClick(DialogInterface dialog, int which) {
	    					dialog.dismiss();
	    					
	    				}
	    			});
	    	    	
	    	    	AlertDialog alert = deleteBuilder.create();
	    	    	alert.show();
                
                return true;
            }
        }); 

		
		final Button createGameButton = (Button) findViewById(R.id.create_new_game_button);		
		createGameButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Log.v(TAG, "User pressed the create new game button");
				Intent createGameIntent = new Intent(GameSelectionActivity.this, CreateGameActivity.class);
				startActivity(createGameIntent);			
			}
		});
		
	}

	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK ) {
	    	
	    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    	builder.setTitle("Log Out?");
	    	builder.setMessage("Are you sure you want to log out?");
	
	    	builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					clearSavedData();
					dialog.dismiss();
					finish();
					return;
				}
			});
	    	
	    	builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					
				}
			});
	    	
	    	AlertDialog alert = builder.create();
	    	alert.show();
	    	
	    }
	    return super.onKeyDown(keyCode, event);
	}	

	
	private void clearSavedData(){
		final SharedPreferences preferences= PreferenceManager.getDefaultSharedPreferences(getBaseContext()); 
		Editor editor = preferences.edit();
		editor.clear(); 
		editor.commit();
	}

	@Override
	protected void onStart() {
		Log.i(TAG, "started the game selection activity");
		super.onStart();
		
		new GetGameListTask().execute();
	}

	@Override
	protected void onRestart() {
		Log.i(TAG, "restarted the game selection activity");
		super.onRestart();
		
		// adapter.clear();
		new GetGameListTask().execute();
	}

	@Override
	protected void onResume() {
		Log.i(TAG, "resumed the game selection activity");
		super.onResume();
		
		new GetGameListTask().execute();
		
	}

	@Override
	protected void onPause() {
		Log.i(TAG, "pause the game selection activity");
		super.onPause();
	}

	@Override
	protected void onStop() {
		Log.i(TAG, "stopped the game selection activity");
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		Log.i(TAG, "destroyed the game selection activity");
		super.onDestroy();
	}
	
	
	
	
	
	
	
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.game_selection, menu);
//		return true;
//	}
//
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//		// Handle action bar item clicks here. The action bar will
//		// automatically handle clicks on the Home/Up button, so long
//		// as you specify a parent activity in AndroidManifest.xml.
//		int id = item.getItemId();
//		if (id == R.id.action_settings) {
//			return true;
//		}
//		return super.onOptionsItemSelected(item);
//	}
}