package edu.utexas.bleiweiss.wherewolf.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import edu.utexas.bleiweiss.wherewolf.WherewolfNetworking;
import edu.utexas.bleiweiss.wherewolf.exceptions.WherewolfNetworkingException;

public class CreateGameRequest extends BasicRequest {
	
	private final Game game;
	
	public CreateGameRequest(String username, String password, Game game){
		super (username, password);
		
		this.game = game;
	}

	public Game getGame() {
		return game;
	}
	
	@Override
	public String getURL() {
		return "/v1/game";
	}
	  
	@Override
	public RequestType getRequestType()
	{
		return RequestType.POST;
	}

	@Override
	public List<NameValuePair> getParameters() {
	List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
	urlParameters.add(new BasicNameValuePair("username", this.username));
	urlParameters.add(new BasicNameValuePair("password", this.password));
	urlParameters.add(new BasicNameValuePair("game_name", this.game.gameName));
	urlParameters.add(new BasicNameValuePair("description", this.game.description));
	return urlParameters;
	}

	public CreateGameResponse processResponse(JSONObject jObject) throws JSONException
	{
		JSONObject jResults = jObject.getJSONObject("results");
		int gameID = jResults.getInt("game_id");

		return new CreateGameResponse("success",
				"Successfully created the game",
				gameID);
	}

	@Override
	public CreateGameResponse execute(WherewolfNetworking net) {
	      
		try {
			JSONObject jObject = net.sendRequest(this);
	          
			String status = jObject.getString("status");
	          
			if (status.equals("success"))
			{
				return new CreateGameResponse("success", "created game");
			} else {
				String errorMessage = jObject.getString("error");
				return new CreateGameResponse("failure", errorMessage);
			}
	          
		} 
		
		catch (WherewolfNetworkingException ex) {
			return new CreateGameResponse("failure", "could not communicate with server.");
		}

		catch (JSONException e) {
			return new CreateGameResponse("failure", "could not parse JSON.");
		}

	      
	}

}
