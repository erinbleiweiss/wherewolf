package edu.utexas.bleiweiss.wherewolf;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.SimpleTimeZone;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Toast;

import com.fortysevendeg.swipelistview.BaseSwipeListViewListener;
import com.fortysevendeg.swipelistview.SwipeListView;

import edu.utexas.bleiweiss.wherewolf.model.GetPlayersRequest;
import edu.utexas.bleiweiss.wherewolf.model.GetPlayersResponse;
import edu.utexas.bleiweiss.wherewolf.model.Player;

public class MainScreenActivity extends Activity {
	private static final String TAG = "MainScreenActivity";
	static SwipeListView swipelistview;
	PlayerAdapter adapter;
	List<Player> playerData;
	int gameId;

	//My boolean
	//boolean isNight;
	
	
	// Background services
	class UpdateTimeTask extends TimerTask
	{
		@Override
		public void run() {
			
			updateTime();
		}
	}
	
	private class GetPlayersTask extends AsyncTask<Void, Integer, GetPlayersResponse>{
		
	@Override
	protected GetPlayersResponse doInBackground(Void... request){
		
		SharedPreferences sharedPreferences = getSharedPreferences("edu.utexas.bleiweiss.wherewolf.prefs", Context.MODE_PRIVATE);
		
		String username = sharedPreferences.getString("username", "");
		String password = sharedPreferences.getString("password", "");
		
		GetPlayersRequest getPlayersRequest = new GetPlayersRequest(username, password, gameId);
		return getPlayersRequest.execute(new WherewolfNetworking());
	}
	
	protected void onPostExecute(GetPlayersResponse result){
		
        if (result.getStatus().equals("success")) {
        	
        	playerData.clear();
        	
			JSONArray jArray = result.getPlayers();
			
			for (int i=0; i<jArray.length(); i++){
				try{
					JSONObject oneObject = jArray.getJSONObject(i);
					// Pulling Items from array
					int playerId = oneObject.getInt("playerid");
					Drawable profPic;
					String profPicUrl = null;
					String playerName = oneObject.getString("playername");
					int numVotes = 0;
					
					if (oneObject.getString("avatar").equals("villagerfemale"))
					{
						profPic = getResources().getDrawable(R.drawable.villagerfemale);
					}
					else
					{
						profPic = getResources().getDrawable(R.drawable.villagermale);
					}
					
					playerData.add(new Player(playerId, profPic, profPicUrl, playerName, numVotes)); 	
					adapter.notifyDataSetChanged();
				}                                    
				catch(JSONException e){
					Log.v(TAG, "JSON Exception was thrown");
				}
			}
			

        } else {
            // do something with bad password
        	Toast.makeText(MainScreenActivity.this, result.getErrorMessage(), Toast.LENGTH_LONG).show();
        }

		
	}
}
	
	
	
	
	
	
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_screen);
		
		WherewolfPreferences pref = new WherewolfPreferences(MainScreenActivity.this);		
		gameId = pref.getCurrentGameID();
		
		// Timer
		Timer timer = new Timer();
		timer.schedule(new UpdateTimeTask(), 1000, 5*60000);
		// timer.schedule(new UpdateTimeTask(), 1000, 5*1000);
		

		new GetPlayersTask().execute();
		
        swipelistview=(SwipeListView)findViewById(R.id.list_of_players);
		playerData=new ArrayList<Player>();
		adapter=new PlayerAdapter (this,R.layout.player_row,playerData);
				
        swipelistview.setSwipeListViewListener(new BaseSwipeListViewListener() {
	         @Override
	         public void onOpened(int position, boolean toRight) {
	         }
		         
	         @Override
	         public void onClosed(int position, boolean fromRight) {
	         }
	 
	         @Override
	         public void onListChanged() {
	         }
	 
	         @Override
	         public void onMove(int position, float x) {
	         }
	 
	         @Override
	         public void onStartOpen(int position, int action, boolean left) {
	         }
	 
	         @Override
	         public void onStartClose(int position, boolean right) {
	             Log.d("swipe", String.format("onStartClose %d", position));
	         }
	 
	         @Override
	         public void onClickFrontView(int position) {
	             Log.d("swipe", String.format("onClickFrontView %d", position));
	             swipelistview.openAnimate(position); //when you touch front view it will open
	         }
	 
	         @Override
	         public void onClickBackView(int position) {
	             Log.d("swipe", String.format("onClickBackView %d", position));
	             swipelistview.closeAnimate(position);//when you touch back view it will close
	         }
	 
	         @Override
	         public void onDismiss(int[] reverseSortedPositions) {
	         }
	 
	     });

	//These are the swipe listview settings. you can change these
	//setting as your requirement
	swipelistview.setSwipeMode(SwipeListView.SWIPE_MODE_LEFT); // there are five swiping modes
	swipelistview.setSwipeActionLeft(SwipeListView.SWIPE_ACTION_REVEAL); //there are four swipe actions
	swipelistview.setSwipeActionRight(SwipeListView.SWIPE_ACTION_REVEAL);
	swipelistview.setOffsetLeft(convertDpToPixel(70f)); // left side offset
	swipelistview.setOffsetRight(convertDpToPixel(0f)); // right side offset
	swipelistview.setAnimationTime(50); // animation time
	swipelistview.setSwipeOpenOnLongPress(false); // enable or disable SwipeOpenOnLongPress

	swipelistview.setAdapter(adapter);
		 

//		playerData.add(new Player(1, getResources().getDrawable(R.drawable.villagermale), "", "Michael", 1));
//		playerData.add(new Player(2, getResources().getDrawable(R.drawable.villagerfemale), "", "Pam", 0));
//		playerData.add(new Player(3, getResources().getDrawable(R.drawable.villagermale), "", "Toby", 2));
//		playerData.add(new Player(4, getResources().getDrawable(R.drawable.villagermale), "", "Jim", 1));
//		playerData.add(new Player(5, getResources().getDrawable(R.drawable.villagerfemale), "", "Angela", 1));
//
//		 
//		adapter.notifyDataSetChanged();

		
		final CircadianWidgetView circadianWidget = (CircadianWidgetView) findViewById(R.id.circadian);
		
		circadianWidget.changeTime(12, this);
		
		final SeekBar sk = (SeekBar) findViewById(R.id.daytime_seekbar);
//		MyOnChangeListener changeListener = new MyOnChangeListener();
//		changeListener.setCircadianViewWidget(circadianWidget);
//		seekbar.setOnSeekBarChangeListener( changeListener );
		
		sk.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			boolean progressIsNight;

		    @Override       
		    public void onStopTrackingTouch(SeekBar seekBar) {      
		        // TODO Auto-generated method stub      
		    }       

		    @Override       
		    public void onStartTrackingTouch(SeekBar seekBar) {     
		        // TODO Auto-generated method stub      
		    }       

		    @Override       
		    public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) {     
		        // TODO Auto-generated method stub      
		    	// progress(time) from 0 - 99, (100)
		    	
		    	// Log.v(TAG, "Set progress to" + Integer.toString(progress));
		    	
				Activity activity = MainScreenActivity.this;

		    	circadianWidget.changeTime(progress, activity);
		    	
		    	progressIsNight = (progress >=5 && progress <= 19);

		    	
		    	if (progressIsNight)
		    	{
		    		adapter.setIsNight(true);
		    		adapter.notifyDataSetChanged();
		    	}
		    	else
		    	{
		    		adapter.setIsNight(false);
		    		adapter.notifyDataSetChanged();
		    	}

		    	
		    	
//		        t1.setTextSize(progress);
//		        Toast.makeText(getApplicationContext(), String.valueOf(progress),Toast.LENGTH_LONG).show();

		    }       
		});

		
		
		
		final Button leaveGameButton = (Button) findViewById(R.id.leavegame_button);		
		leaveGameButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Log.v(TAG, "User pressed the register button");
				
				
				AlertDialog.Builder builder = new AlertDialog.Builder(MainScreenActivity.this);
				builder.setMessage("Are you sure you want to leave this game?")
				   .setCancelable(false)
				   .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				       public void onClick(DialogInterface dialog, int id) {
				        	WherewolfPreferences pref = new WherewolfPreferences(MainScreenActivity.this);
				        	pref.setCurrentGameID(0);
				            Intent intent= new Intent(MainScreenActivity.this, GameSelectionActivity.class);
				            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//				            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				            startActivity(intent);
				            MainScreenActivity.this.finish();
				       }
				   })
				   .setNegativeButton("No", new DialogInterface.OnClickListener() {
				       public void onClick(DialogInterface dialog, int id) {
				            dialog.cancel();
				       }
				   });
				AlertDialog alert = builder.create();
				alert.show();
				
			}
		});		
		
		
		
		
		
		
		
		
		final Button logoutButton = (Button) findViewById(R.id.logout_button);		
		logoutButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Log.v(TAG, "User pressed the register button");
				
				
				AlertDialog.Builder builder = new AlertDialog.Builder(MainScreenActivity.this);
				builder.setMessage("Are you sure you want to exit?")
				   .setCancelable(false)
				   .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				       public void onClick(DialogInterface dialog, int id) {
				    	    clearPassword();
							Intent explicitIntent = new Intent(MainScreenActivity.this, LoginActivity.class);
							explicitIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
							explicitIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							startActivity(explicitIntent);
				       }
				   })
				   .setNegativeButton("No", new DialogInterface.OnClickListener() {
				       public void onClick(DialogInterface dialog, int id) {
				            dialog.cancel();
				       }
				   });
				AlertDialog alert = builder.create();
				alert.show();
				
			}
		});		
		
		
	}

	public int convertDpToPixel(float dp) {
	       DisplayMetrics metrics = getResources().getDisplayMetrics();
	       float px = dp * (metrics.densityDpi / 160f);
	       return (int) px;
	}
	
	
	private void clearPassword(){
    	WherewolfPreferences pref = new WherewolfPreferences(MainScreenActivity.this);
    	pref.setCreds("", "");
	}
	
	void updateTime()
	{
		Log.v(TAG, "updating time");
		
		final CircadianWidgetView circadianWidget = (CircadianWidgetView) findViewById(R.id.circadian);
		final SeekBar sk = (SeekBar) findViewById(R.id.daytime_seekbar);
		
		WherewolfPreferences pref = new WherewolfPreferences(this);
		Long currentTime = pref.getTime();

		// GregorianCalendar cal = new GregorianCalendar();
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("America/Chicago"));
        cal.setTimeInMillis(currentTime * 1000);
        final int hour = cal.get(Calendar.HOUR_OF_DAY);
        final int min = cal.get(Calendar.MINUTE);
        double t = (double)hour + (double)min/60.0f;
        
        
//		Log.v(TAG, Integer.toString(hour));
//		Log.v(TAG, Integer.toString(min));
//		
//		Log.v(TAG, Double.toString(t));
//		
	
//		Activity activity = MainScreenActivity.this;
//		activity.runOnUiThread(new Runnable() {
//			  public void run() {
//				Toast.makeText(MainScreenActivity.this, "Time: " + Integer.toString(hour) + ":" + Integer.toString(min), Toast.LENGTH_LONG).show();
//			  }
//			});				
		circadianWidget.changeTime((int) t, activity);
		sk.setProgress((int) t);
	}
	
	
	
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.game_selection, menu);
//		return true;
//	}
//
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//		// Handle action bar item clicks here. The action bar will
//		// automatically handle clicks on the Home/Up button, so long
//		// as you specify a parent activity in AndroidManifest.xml.
//		int id = item.getItemId();
//		if (id == R.id.action_settings) {
//			return true;
//		}
//		return super.onOptionsItemSelected(item);
//	}
	
}