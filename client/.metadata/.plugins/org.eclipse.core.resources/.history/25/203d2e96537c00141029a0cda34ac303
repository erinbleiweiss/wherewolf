package edu.utexas.bleiweiss.wherewolf;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Base64;
import android.util.Log;
import edu.utexas.bleiweiss.wherewolf.exceptions.WherewolfNetworkingException;
import edu.utexas.bleiweiss.wherewolf.model.BasicRequest;
import edu.utexas.bleiweiss.wherewolf.model.BasicRequest.RequestType;

public class WherewolfNetworking {

	// when you submit, replace this IP address with EC2 Instance (load balancer?)
	// Amazon EC2
	//private static final String fullhost = "http://54.191.43.168:5000";
	// private static final String fullhost = "http://wherewolf-loadbalancer-280752479.us-west-2.elb.amazonaws.com";
	
	// Home
	private static final String fullhost = "http://192.168.1.77:5000";
	
	// UT
	// private static final String fullhost = "http://10.147.159.224:5000";
	
	// IP for built-in emulator that's not Genymotion
	// private static final String fullhost = "http://10.0.2.2:5000/";

	private static final String TAG = "WhereWolfNetworking";
	
	
	public WherewolfNetworking(){
		
	}
	
	public JSONObject sendRequest(BasicRequest basicRequest)
	          throws WherewolfNetworkingException {
	      
	      InputStream inputStream = null;
	      String result = null;

	      String url = fullhost + basicRequest.getURL();
	      RequestType requestType = basicRequest.getRequestType();
	      List<NameValuePair> payload = basicRequest.getParameters();
	      String username = basicRequest.getUsername();
	      String password = basicRequest.getPassword();

	      try { 
	    	  
	          final DefaultHttpClient httpClient = new DefaultHttpClient();

	          // HttpUriRequest request;
	          HttpResponse response;

	          HttpUriRequest request;

	          if (basicRequest.getRequestType() == RequestType.GET) {
	              request = new HttpGet(url);
	              
	              request.setHeader("Content-type", "application/json");
	              // add authentication stuff here.

	          } else if (requestType == RequestType.POST) {

	              HttpPost postRequest = new HttpPost(url);
	              postRequest.setHeader("Content-type",
	                      "application/x-www-form-urlencoded");

	              if (payload!=null) {

	                  postRequest.setEntity(new UrlEncodedFormEntity(payload));
	              }
	              
	              request = postRequest;
	              

	          } else if (requestType == RequestType.DELETE) {
	              
	              HttpDelete deleteRequest = new HttpDelete(url);
	          
	              request = deleteRequest;
	              request.setHeader("Content-type", "application/json");
	              
	          } else if (requestType == RequestType.PUT) {
	              
	              request = new HttpPut(url);
	              request.setHeader("Content-type", "application/json");
	              
	                  
	          } else {
	              throw new WherewolfNetworkingException(
	                      "Does not support the HTTP request type");
	          }

	          
	          
	          

	          if (!username.equals("")) {
	              String authorizationString = "Basic "
	                      + Base64.encodeToString(
	                              (username + ":" + password).getBytes(),
	                              Base64.NO_WRAP);
	              request.setHeader("Authorization", authorizationString);
	          }
	          
	          response = httpClient.execute(request);

	          HttpEntity entity = response.getEntity();

	          inputStream = entity.getContent();

	          BufferedReader reader = new BufferedReader(new InputStreamReader(
	                  inputStream, "UTF-8"), 8);
	          StringBuilder sb = new StringBuilder();

	          String line = null;
	          while ((line = reader.readLine()) != null) {
	              sb.append(line + "\n");
	          }

	          result = sb.toString();

	          try {

	              JSONObject json = new JSONObject(result);
	              return json;

	          } catch (JSONException ex) {
	              throw new WherewolfNetworkingException("Could not parse JSON");
	          }

	      } catch (Exception e) {
	          Log.e(TAG, "Problem with response from server" + e.toString());

	      } finally {
	          try {
	              if (inputStream != null)
	                  inputStream.close();
	          } catch (Exception ex) {
	              throw new WherewolfNetworkingException("Network problem");
	          }
	      }

	      throw new WherewolfNetworkingException("Network problem");

	  }
	  
		
	// ************************************ //
	public String getJsonFromServer(String url, RequestType requestType,
            String username, String password, List<NameValuePair> payload) {

        InputStream inputStream = null;
        String result = null;

        try {

            final DefaultHttpClient httpClient = new DefaultHttpClient();

            // HttpUriRequest request;
            HttpResponse response;

            if (requestType == RequestType.GET) {
                HttpGet getRequest = new HttpGet(url);
                getRequest.setHeader("Content-type", "application/json");

                // add authentication stuff here.
                if (!username.equals("")) {
                    String authorizationString = "Basic "
                            + Base64.encodeToString(
                                    (username + ":" + password).getBytes(),
                                    Base64.NO_WRAP);
                    getRequest.setHeader("Authorization", authorizationString);
                }

                response = httpClient.execute(getRequest);

            } else if (requestType == RequestType.POST) {

                HttpPost postRequest = new HttpPost(url);
                postRequest.setHeader("Content-type",
                        "application/x-www-form-urlencoded");

                if (!payload.equals("")) {

                    postRequest.setEntity(new UrlEncodedFormEntity(payload));
                }

                // add authentication stuff here.
                if (!username.equals("")) {
                    String authorizationString = "Basic "
                            + Base64.encodeToString(
                                    (username + ":" + password).getBytes(),
                                    Base64.NO_WRAP);
                    postRequest.setHeader("Authorization", authorizationString);
                }

                response = httpClient.execute(postRequest);

            } else {
                return null;
            }

            // execute the request here.
            // HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();

            inputStream = entity.getContent();

            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    inputStream, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }

            result = sb.toString();
        } catch (Exception e) {
            Log.e(TAG, "Problem with response from server" + e.toString());

        } finally {
            try {
                if (inputStream != null)
                    inputStream.close();
            } catch (Exception ex) {
            }
        }

        return result;
    }	
	// ************************************ //


	
}
